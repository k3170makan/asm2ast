# asm2ast

Some experiments in generating my own IR language for binary programs. asm2ast takes in the objdump output of a binary and at the moment it spits out a verbose syntax tree. 

# Usage

```
./build/asm2ast.cc objdump-output
example: ./build/asm2ast.cc ./samples/ntfsinfo.sample

[*] parsing source code '../samples/ntfsinfo.sample'
[7]< >{LexerState::init_op_names()}> initializing op names...
[7]< >{Parser::Parser}> [i] initializing source code stream
[7]< >{LexerState::LexerState}> [i] successfully fopen'd sourced file
[1]< >{LexerState::lex_token()}> [i] TOKEN => OP [push]
[2]<_>{LexerState::lex_token()}> [i] TOKEN => REG [r15]
[2]<_>{LexerState::push_op_parser()}> pushAST [ regAST[ 'r15']]
[1]< >{LexerState::lex_token()}> [i] TOKEN => OP [mov]
...
[6]<]>{Parser::memref_parser}> memRef[  QWORD binOpAST[{+}  regAST[ 'rip']  numAST[ '0x92a2']]]
[6]<]>{LexerState::lea_op_parser()}> leaAST[ regAST[ 'r14']  memRef[  QWORD binOpAST[{+}  regAST[ 'rip']  numAST[ '0x92a2']]]]
...
[6]<]>{Parser::memref_parser}> memRef[  DWORD binOpAST[{+}  regAST[ 'r14']  binOpAST[{*}  regAST[ 'rdx']  numAST[ '0x4']]]]
[6]<]>{LexerState::mov_op_parser()}> movAST[ regAST[ 'rdx']  memRef[  DWORD binOpAST[{+}  regAST[ 'r14']  binOpAST[{*}  regAST[ 'rdx']  numAST[ '0x4']]]]]
```

# Build

Pretty straightfoward compile journey, just use cmake and make:
```
cd asm2ast
mkdir build
cd build
cmake ..
make
```
