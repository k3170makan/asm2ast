#include <vector>
#include <math.h>
#include <map>
#include <iostream>
#include <mutex>
#include <algorithm>
//https://x86.puri.sm/

using namespace std;

enum token_type {
	EOF_TOKEN = 0,
	OP_TOKEN = 1,
	REG_TOKEN = 2,
	SEGREG_TOKEN = 3,
	MEMREF_TOKEN = 4,
	MEMSIZE_TOKEN = 5,
	NUMERIC_TOKEN = 6,
	BEGIN_TOKEN = 7
};

enum reg_names{
	rax_reg = 1,
	rbx_reg,
	rcx_reg,
	rdx_reg,
	rbp_reg,
	rsp_reg,
	rip_reg,
	rdi_reg,
	rsi_reg,
	r8_reg,
	r9_reg,
	r10_reg,
	r11_reg,
	r12_reg,
	r12d_reg,
	r13_reg,
	r14_reg,
	r15_reg,
	eax_reg,
	ebx_reg,
	ecx_reg,
	edx_reg,
	edi_reg,
	esi_reg
};

//token char function message
#define LOG_FORMAT "[%d]<%c>{%s}> %s\n"

/* LexerState - used to record the state of the lexer i.e. the token's seen
		the current token, source metrics etc.*/


class LexerState { 
	public:
		std::string get_current_function_name();
		std::string get_current_source_file_name();
		std::string identity_string;
		std::string numeric_string;
		int numeric_value;
		std::mutex cursor_mutex;
		std::string _source_filename;
		FILE* _source_stream;
		char __current_char;
		char __current_token;
		unsigned long __current_pos;
		std::vector<char> _token_list;

		LexerState(std::string &source_filename);
		virtual ~LexerState() {};
		LexerState() {};

		int __init_source();
		char __next_char();
		char __get_current_token();
		void __log_format(const char*, const char*); //[current token](pos)<char>{Parser::__}>
		void __skip_spaces();
		char get_current_token();
		char lex_token();
		void next_char();
		unsigned long get_cursor_position();

		vector<std::string> token_names;
		vector<std::string> op_names;
		vector<std::string> reg_names;
		vector<std::string> segreg_names;
		vector<std::string> memsize_names;
	
		//map<const char*,BaseAST* (LexerState::*)(void)> op_dispatch;	
		
		void __init_token_names();
		void __init_op_names();
		void __init_reg_names();
		void __init_segreg_names();
		void __init_memsize_names();
		void _lex_numeric();

		bool is_memsize_name(std::string &);
		bool is_reg_name(std::string &);
		bool is_segreg_name(std::string &);
		bool is_op_string(std::string &);
		bool is_binop(char );

		unsigned short get_size(BaseAST *);
		unsigned int get_reg(RegAST*);

	
		/*these should move to the parser*/
};

class Parser : public LexerState {
	public:

		std::vector<BaseAST*> parse();
		MemRefAST* memref_parser(std::string);
		BinOpAST* binop_parser(unsigned int, BaseAST*);
		BaseAST* base_parser();
		BaseAST*  __dispatch_op();
		MemRefAST*  __dispatch_memref();
		void __tokenizer_test();
		void __base_parser_test();
		std::string _build_ast_string(BaseAST*);
		virtual ~Parser() {};
		Parser(std::string &filename);
		void __init();
		void __init_precedence();
		std::map<char, int> binops_precedence;
		unsigned int get_op_precedence(char);
		std::string dump_str(BaseAST* );
		BaseAST* get_op(std::string &);
		BaseAST* sign_extend(bool, BaseAST* );



		std::vector<BaseAST*> op_arg_parser_2(void);
		BaseAST* op_arg_parser_1(void);
		RegAST* reg_parser();
		SegRegAST* segreg_parser();
		NumericAST* numeric_parser(bool);
		BaseAST* adc_op_parser(void);
		BaseAST* add_op_parser(void);
		BaseAST* addr32_op_parser(void);

		CdqeAST* cdqe_op_parser(void);
		CbwAST* cbw_op_parser(void);
		CwdeAST* cwde_op_parser(void);

		BaseAST* cwd_op_parser(void);
		CdqAST* cdq_op_parser(void);
		BaseAST* cqo_op_parser(void);


		CallAST* call_op_parser(void);
		BaseAST* clc_op_parser(void);
		BaseAST* cld_op_parser(void);
		BaseAST* cli_op_parser(void);
		BaseAST* cmc_op_parser(void);

		BaseAST* cmovae_op_parser(void);
		BaseAST* cmova_op_parser(void);
		BaseAST* cmovbe_op_parser(void);
		BaseAST* cmovb_op_parser(void);
		BaseAST* cmove_op_parser(void);
		BaseAST* cmovge_op_parser(void);
		BaseAST* cmovg_op_parser(void);
		BaseAST* cmovle_op_parser(void);
		BaseAST* cmovl_op_parser(void);
		BaseAST* cmovne_op_parser(void);
		BaseAST* cmovns_op_parser(void);
		BaseAST* cmovs_op_parser(void);

		CmpAST* cmp_op_parser(void);
		CmpsAST* cmps_op_parser(void);
		CmpsbAST* cmpsb_op_parser(void);
		CmpswAST* cmpsw_op_parser(void);
		CmpsdAST* cmpsd_op_parser(void);
		CmpsqAST* cmpsq_op_parser(void);

		BaseAST* cs_op_parser(void);
		BaseAST* dec_op_parser(void);
		BaseAST* div_op_parser(void);
		BaseAST* ds_op_parser(void);
		BaseAST* es_op_parser(void);
		BaseAST* fcom_op_parser(void);
		BaseAST* fcomip_op_parser(void);
		BaseAST* fcomp_op_parser(void);
		BaseAST* fdiv_op_parser(void);
		BaseAST* fdivp_op_parser(void);
		BaseAST* fdivr_op_parser(void);
		BaseAST* fdivrp_op_parser(void);
		BaseAST* fiadd_op_parser(void);
		BaseAST* ficomp_op_parser(void);
		BaseAST* fidivr_op_parser(void);
		BaseAST* fidiv_op_parser(void);
		BaseAST* fild_op_parser(void);
		BaseAST* fimul_op_parser(void);
		BaseAST* fisttp_op_parser(void);
		BaseAST* fld_op_parser(void);
		BaseAST* fldz_op_parser(void);
		BaseAST* fmul_op_parser(void);
		BaseAST* fprem_op_parser(void);
		BaseAST* fprem1_op_parser(void);
		BaseAST* fs_op_parser(void);
		BaseAST* fscale_op_parser(void);
		BaseAST* fsqrt_op_parser(void);
		BaseAST* fstp_op_parser(void);
		BaseAST* fsub_op_parser(void);
		BaseAST* fsubp_op_parser(void);
		BaseAST* fsubr_op_parser(void);
		BaseAST* fucompp_op_parser(void);
		BaseAST* fwait_op_parser(void);
		BaseAST* icebp_op_parser(void);
		BaseAST* idiv_op_parser(void);
		BaseAST* imul_op_parser(void);
		BaseAST* in_op_parser(void);
		BaseAST* inc_op_parser(void);
		BaseAST* ins_op_parser(void);
		BaseAST* int_op_parser(void);
		BaseAST* int3_op_parser(void);

		JaAST* ja_op_parser(void);
		JaeAST* jae_op_parser(void);
		JbAST* jb_op_parser(void);
		JbeAST* jbe_op_parser(void);
		JeAST* je_op_parser(void);
		JgAST* jg_op_parser(void);
		JgeAST* jge_op_parser(void);
		JlAST* jl_op_parser(void);
		JleAST* jle_op_parser(void);
		JmpAST* jmp_op_parser(void);
		JneAST* jne_op_parser(void);
		JnoAST* jno_op_parser(void);
		JnpAST* jnp_op_parser(void);
		JnsAST* jns_op_parser(void);
		JoAST* jo_op_parser(void);
		JpAST* jp_op_parser(void);
		JsAST* js_op_parser(void);

		BaseAST* jrcxz_op_parser(void);
		BaseAST* lahf_op_parser(void);
		BaseAST* lea_op_parser(void);
		BaseAST* leave_op_parser(void);
		BaseAST* lock_op_parser(void);
		BaseAST* lods_op_parser(void);
		BaseAST* loop_op_parser(void);
		BaseAST* loope_op_parser(void);
		BaseAST* loopne_op_parser(void);
		BaseAST* lsl_op_parser(void);
		BaseAST* maskmovq_op_parser(void);
		MovAST* movabs_op_parser(void);
		MovAST* mov_op_parser(void);
		MovAST* movaps_op_parser(void);
		MovAST* movdqa_op_parser(void);
		MovAST* movdqu_op_parser(void);
		MovAST* movs_op_parser(void);
		MovAST* movsxd_op_parser(void);
		MovAST* movsx_op_parser(void);
		MovAST* movups_op_parser(void);
		MovAST* movzx_op_parser(void);
		MulAST* mul_op_parser(void);
		NegAST* neg_op_parser(void);
		NopAST* nop_op_parser(void);
		NotAST* not_op_parser(void);
		BaseAST* or_op_parser(void);
		BaseAST* out_op_parser(void);
		BaseAST* outs_op_parser(void);
		BaseAST* paddb_op_parser(void);
		BaseAST* pop_op_parser(void);
		BaseAST* popf_op_parser(void);
		BaseAST* psadbw_op_parser(void);
		BaseAST* psubb_op_parser(void);
		BaseAST* psubd_op_parser(void);
		BaseAST* psubw_op_parser(void);
		PushAST* push_op_parser(void);
		BaseAST* pushf_op_parser(void);
		BaseAST* pxor_op_parser(void);
		BaseAST* rcl_op_parser(void);
		BaseAST* rcr_op_parser(void);
		BaseAST* rep_op_parser(void);
		BaseAST* repnz_op_parser(void);
		BaseAST* repz_op_parser(void);
		BaseAST* ret_op_parser(void);
		BaseAST* retf_op_parser(void);
		BaseAST* rex_op_parser(void);
		BaseAST* rol_op_parser(void);
		BaseAST* ror_op_parser(void);
		BaseAST* sahf_op_parser(void);
		BaseAST* sar_op_parser(void);
		BaseAST* sbb_op_parser(void);
		BaseAST* scas_op_parser(void);
		BaseAST* seta_op_parser(void);
		BaseAST* setb_op_parser(void);
		BaseAST* setbe_op_parser(void);
		BaseAST* sete_op_parser(void);
		BaseAST* setg_op_parser(void);
		BaseAST* setge_op_parser(void);
		BaseAST* setl_op_parser(void);
		BaseAST* setle_op_parser(void);
		BaseAST* setne_op_parser(void);
		BaseAST* sgdt_op_parser(void);
		BaseAST* shl_op_parser(void);
		BaseAST* shr_op_parser(void);
		BaseAST* sidt_op_parser(void);
		BaseAST* sldt_op_parser(void);
		BaseAST* ss_op_parser(void);
		BaseAST* stc_op_parser(void);
		BaseAST* std_op_parser(void);
		BaseAST* sti_op_parser(void);
		BaseAST* stos_op_parser(void);
		BaseAST* str_op_parser(void);
		BaseAST* sub_op_parser(void);
		BaseAST* syscall_op_parser(void);
		BaseAST* xchg_op_parser(void);
		BaseAST* xlat_op_parser(void);
		XorAST* xor_op_parser(void);

		void get_op_size(BaseAST*);

};

unsigned int Parser::get_op_precedence(char op){
	return this->binops_precedence[op];
}

void Parser::__init_precedence(){

	this->binops_precedence['-'] = 1;
        this->binops_precedence['+'] = 2;
        this->binops_precedence['/'] = 3;
        this->binops_precedence['*'] = 4;
} 

BaseAST* Parser::get_op(std::string &op){

	if (op.compare("adc") == 0) { return this->adc_op_parser(); }
	if (op.compare("addr32") == 0) { return this->addr32_op_parser(); }
	if (op.compare("add") == 0) { return this->add_op_parser(); }
	if (op.compare("cdqe") == 0) { return this->cdqe_op_parser(); }
	if (op.compare("call") == 0) { return this->call_op_parser(); }
	if (op.compare("clc") == 0) { return this->clc_op_parser(); }
	if (op.compare("cld") == 0) { return this->cld_op_parser(); }
	if (op.compare("cli") == 0) { return this->cli_op_parser(); }
	if (op.compare("cmc") == 0) { return this->cmc_op_parser(); }
	if (op.compare("cmovae") == 0) { return this->cmovae_op_parser(); }
	if (op.compare("cmova") == 0) { return this->cmova_op_parser(); }
	if (op.compare("cmovbe") == 0) { return this->cmovbe_op_parser(); }
	if (op.compare("cmovb") == 0) { return this->cmovb_op_parser(); }
	if (op.compare("cmove") == 0) { return this->cmove_op_parser(); }
	if (op.compare("cmovg") == 0) { return this->cmovg_op_parser(); }
	if (op.compare("cmovge") == 0) { return this->cmovge_op_parser(); }
	if (op.compare("cmovg") == 0) { return this->cmovg_op_parser(); }
	if (op.compare("cmovl") == 0) { return this->cmovl_op_parser(); }
	if (op.compare("cmovle") == 0) { return this->cmovle_op_parser(); }
	if (op.compare("cmovl") == 0) { return this->cmovl_op_parser(); }
	if (op.compare("cmovne") == 0) { return this->cmovne_op_parser(); }
	if (op.compare("cmovns") == 0) { return this->cmovns_op_parser(); }
	if (op.compare("cmovs") == 0) { return this->cmovs_op_parser(); }
	if (op.compare("cmps") == 0) { return this->cmps_op_parser(); }
	if (op.compare("cmp") == 0) { return this->cmp_op_parser(); }
	if (op.compare("cqo") == 0) { return this->cqo_op_parser(); }
	//if (op.compare("cs") == 0) { return this->cs_op_parser(); }
	if (op.compare("cwde") == 0) { return this->cwde_op_parser(); }
	if (op.compare("dec") == 0) { return this->dec_op_parser(); }
	if (op.compare("div") == 0) { return this->div_op_parser(); }
	//if (op.compare("ds") == 0) { return this->ds_op_parser(); }
	//if (op.compare("es") == 0) { return this->es_op_parser(); }
	if (op.compare("fcom") == 0) { return this->fcom_op_parser(); }
	if (op.compare("fcomip") == 0) { return this->fcomip_op_parser(); }
	if (op.compare("fcomp") == 0) { return this->fcomp_op_parser(); }
	if (op.compare("fcom") == 0) { return this->fcom_op_parser(); }
	if (op.compare("fdivp") == 0) { return this->fdivp_op_parser(); }
	if (op.compare("fdiv") == 0) { return this->fdiv_op_parser(); }
	if (op.compare("fdivr") == 0) { return this->fdivr_op_parser(); }
	if (op.compare("fdivrp") == 0) { return this->fdivrp_op_parser(); }
	if (op.compare("fdivr") == 0) { return this->fdivr_op_parser(); }
	if (op.compare("fiadd") == 0) { return this->fiadd_op_parser(); }
	if (op.compare("ficomp") == 0) { return this->ficomp_op_parser(); }
	if (op.compare("fidivr") == 0) { return this->fidivr_op_parser(); }
	if (op.compare("fidiv") == 0) { return this->fidiv_op_parser(); }
	if (op.compare("fild") == 0) { return this->fild_op_parser(); }
	if (op.compare("fimul") == 0) { return this->fimul_op_parser(); }
	if (op.compare("fisttp") == 0) { return this->fisttp_op_parser(); }
	if (op.compare("fld") == 0) { return this->fld_op_parser(); }
	if (op.compare("fldz") == 0) { return this->fldz_op_parser(); }
	if (op.compare("fmul") == 0) { return this->fmul_op_parser(); }
	if (op.compare("fprem") == 0) { return this->fprem_op_parser(); }
	if (op.compare("fprem1") == 0) { return this->fprem1_op_parser(); }
	if (op.compare("fscale") == 0) { return this->fscale_op_parser(); }
	if (op.compare("fsqrt") == 0) { return this->fsqrt_op_parser(); }
	if (op.compare("fs") == 0) { return this->fs_op_parser(); }
	if (op.compare("fstp") == 0) { return this->fstp_op_parser(); }
	if (op.compare("fsubp") == 0) { return this->fsubp_op_parser(); }
	if (op.compare("fsubr") == 0) { return this->fsubr_op_parser(); }
	if (op.compare("fsub") == 0) { return this->fsub_op_parser(); }
	if (op.compare("fucompp") == 0) { return this->fucompp_op_parser(); }
	if (op.compare("fwait") == 0) { return this->fwait_op_parser(); }
	if (op.compare("icebp") == 0) { return this->icebp_op_parser(); }
	if (op.compare("idiv") == 0) { return this->idiv_op_parser(); }
	if (op.compare("imul") == 0) { return this->imul_op_parser(); }
	if (op.compare("inc") == 0) { return this->inc_op_parser(); }
	if (op.compare("in") == 0) { return this->in_op_parser(); }
	if (op.compare("ins") == 0) { return this->ins_op_parser(); }
	if (op.compare("int") == 0) { return this->int_op_parser(); }
	if (op.compare("int3") == 0) { return this->int3_op_parser(); }
	if (op.compare("ja") == 0) { return this->ja_op_parser(); }
	if (op.compare("jae") == 0) { return this->jae_op_parser(); }
	if (op.compare("jb") == 0) { return this->jb_op_parser(); }
	if (op.compare("jbe") == 0) { return this->jbe_op_parser(); }
	if (op.compare("je") == 0) { return this->je_op_parser(); }
	if (op.compare("jge") == 0) { return this->jge_op_parser(); }
	if (op.compare("jg") == 0) { return this->jg_op_parser(); }
	if (op.compare("jl") == 0) { return this->jl_op_parser(); }
	if (op.compare("jle") == 0) { return this->jle_op_parser(); }
	if (op.compare("jmp") == 0) { return this->jmp_op_parser(); }
	if (op.compare("jne") == 0) { return this->jne_op_parser(); }
	if (op.compare("jno") == 0) { return this->jno_op_parser(); }
	if (op.compare("jnp") == 0) { return this->jnp_op_parser(); }
	if (op.compare("jns") == 0) { return this->jns_op_parser(); }
	if (op.compare("jo") == 0) { return this->jo_op_parser(); }
	if (op.compare("jp") == 0) { return this->jp_op_parser(); }
	if (op.compare("jrcxz") == 0) { return this->jrcxz_op_parser(); }
	if (op.compare("js") == 0) { return this->js_op_parser(); }
	if (op.compare("lahf") == 0) { return this->lahf_op_parser(); }
	if (op.compare("lea") == 0) { return this->lea_op_parser(); }
	if (op.compare("leave") == 0) { return this->leave_op_parser(); }
	if (op.compare("lock") == 0) { return this->lock_op_parser(); }
	if (op.compare("lods") == 0) { return this->lods_op_parser(); }
	if (op.compare("loope") == 0) { return this->loope_op_parser(); }
	if (op.compare("loop") == 0) { return this->loop_op_parser(); }
	if (op.compare("loopne") == 0) { return this->loopne_op_parser(); }
	if (op.compare("lsl") == 0) { return this->lsl_op_parser(); }
	if (op.compare("maskmovq") == 0) { return this->maskmovq_op_parser(); }
	if (op.compare("movabs") == 0) { return this->movabs_op_parser(); }
	if (op.compare("movaps") == 0) { return this->movaps_op_parser(); }
	if (op.compare("movdqa") == 0) { return this->movdqa_op_parser(); }
	if (op.compare("movdqu") == 0) { return this->movdqu_op_parser(); }
	if (op.compare("movs") == 0) { return this->movs_op_parser(); }
	if (op.compare("movsxd") == 0) { return this->movsxd_op_parser(); }
	if (op.compare("movsx") == 0) { return this->movsx_op_parser(); }
	if (op.compare("movups") == 0) { return this->movups_op_parser(); }
	if (op.compare("mov") == 0) { return this->mov_op_parser(); }
	if (op.compare("movzx") == 0) { return this->movzx_op_parser(); }
	if (op.compare("mul") == 0) { return this->mul_op_parser(); }
	if (op.compare("neg") == 0) { return this->neg_op_parser(); }
	if (op.compare("nop") == 0) { return this->nop_op_parser(); }
	if (op.compare("not") == 0) { return this->not_op_parser(); }
	if (op.compare("or") == 0) { return this->or_op_parser(); }
	if (op.compare("out") == 0) { return this->out_op_parser(); }
	if (op.compare("outs") == 0) { return this->outs_op_parser(); }
	if (op.compare("paddb") == 0) { return this->paddb_op_parser(); }
	if (op.compare("popf") == 0) { return this->popf_op_parser(); }
	if (op.compare("pop") == 0) { return this->pop_op_parser(); }
	if (op.compare("psadbw") == 0) { return this->psadbw_op_parser(); }
	if (op.compare("psubb") == 0) { return this->psubb_op_parser(); }
	if (op.compare("psubd") == 0) { return this->psubd_op_parser(); }
	if (op.compare("psubw") == 0) { return this->psubw_op_parser(); }
	if (op.compare("push") == 0) { return this->push_op_parser(); }
	if (op.compare("pushf") == 0) { return this->pushf_op_parser(); }
	if (op.compare("pxor") == 0) { return this->pxor_op_parser(); }
	if (op.compare("rcl") == 0) { return this->rcl_op_parser(); }
	if (op.compare("rcr") == 0) { return this->rcr_op_parser(); }
	if (op.compare("repnz") == 0) { return this->repnz_op_parser(); }
	if (op.compare("rep") == 0) { return this->rep_op_parser(); }
	if (op.compare("repz") == 0) { return this->repz_op_parser(); }
	if (op.compare("ret") == 0) { return this->ret_op_parser(); }
	if (op.compare("retf") == 0) { return this->retf_op_parser(); }
	if (op.compare("rex") == 0) { return this->rex_op_parser(); }
	if (op.compare("rol") == 0) { return this->rol_op_parser(); }
	if (op.compare("ror") == 0) { return this->ror_op_parser(); }
	if (op.compare("sahf") == 0) { return this->sahf_op_parser(); }
	if (op.compare("sar") == 0) { return this->sar_op_parser(); }
	if (op.compare("sbb") == 0) { return this->sbb_op_parser(); }
	if (op.compare("scas") == 0) { return this->scas_op_parser(); }
	if (op.compare("seta") == 0) { return this->seta_op_parser(); }
	if (op.compare("setbe") == 0) { return this->setbe_op_parser(); }
	if (op.compare("setb") == 0) { return this->setb_op_parser(); }
	if (op.compare("sete") == 0) { return this->sete_op_parser(); }
	if (op.compare("setge") == 0) { return this->setge_op_parser(); }
	if (op.compare("setg") == 0) { return this->setg_op_parser(); }
	if (op.compare("setle") == 0) { return this->setle_op_parser(); }
	if (op.compare("setl") == 0) { return this->setl_op_parser(); }
	if (op.compare("setne") == 0) { return this->setne_op_parser(); }
	if (op.compare("sgdt") == 0) { return this->sgdt_op_parser(); }
	if (op.compare("shl") == 0) { return this->shl_op_parser(); }
	if (op.compare("shr") == 0) { return this->shr_op_parser(); }
	if (op.compare("sidt") == 0) { return this->sidt_op_parser(); }
	if (op.compare("sldt") == 0) { return this->sldt_op_parser(); }
	if (op.compare("ss") == 0) { return this->ss_op_parser(); }
	if (op.compare("stc") == 0) { return this->stc_op_parser(); }
	if (op.compare("std") == 0) { return this->std_op_parser(); }
	if (op.compare("sti") == 0) { return this->sti_op_parser(); }
	if (op.compare("stos") == 0) { return this->stos_op_parser(); }
	if (op.compare("str") == 0) { return this->str_op_parser(); }
	if (op.compare("sub") == 0) { return this->sub_op_parser(); }
	if (op.compare("syscall") == 0) { return this->syscall_op_parser(); }
	if (op.compare("xchg") == 0) { return this->xchg_op_parser(); }
	if (op.compare("xlat") == 0) { return this->xlat_op_parser(); }
	if (op.compare("xor") == 0) { return this->xor_op_parser(); }

	return NULL;
}

NumericAST* Parser::numeric_parser(bool post){
	NumericAST* _num;
	_num =  new NumericAST(this->numeric_string);
	return _num;
}
RegAST* Parser::reg_parser(){
	return new RegAST(this->identity_string);
}
SegRegAST* Parser::segreg_parser(){
	//identity string points to seg reg name and cur char is at ':', we should see the offset next

	//this->__log_format("Parser::segreg_parser","[i] parsing segreg...");

	NumericAST* _offset;
	SegRegAST* segreg;
	std::string _name = this->identity_string;

	if (this->__current_char == ':'){
		this->next_char();
	}
	else{
		return NULL;
	}

	this->lex_token();
	if (this->__current_token != NUMERIC_TOKEN){
		return NULL;
	}

	_offset = this->numeric_parser(false);	
	segreg = new SegRegAST(_name,_offset);

	this->__log_format("Parser::segreg_parser",segreg->dump_str().c_str());
	return segreg;
	
}

BaseAST* Parser::op_arg_parser_1(void){

	BaseAST *arg;
	this->__log_format("Parser::op_arg_parser_2","[i] parsing op_arg single...");	
	this->lex_token();
	switch(this->__current_token){
		case REG_TOKEN:
			arg = (RegAST*) this->reg_parser();break;			
		case NUMERIC_TOKEN:
			arg = (NumericAST*) this->numeric_parser(false);break;	
		case MEMSIZE_TOKEN:
			arg = (MemRefAST*) this->memref_parser(this->identity_string);break;
		case MEMREF_TOKEN:
			arg = (MemRefAST*) this->memref_parser("QWORD");break;
	}

	return arg;
}
std::vector<BaseAST*> Parser::op_arg_parser_2(void){
	std::vector<BaseAST*> _args;

	BaseAST *src, *dst;

	this->__log_format("Parser::op_arg_parser_2","[i] parsing op_arg double...");	
	this->lex_token();

	if(this->__current_char != ',' && this->__current_token != MEMSIZE_TOKEN && this->__current_token != MEMREF_TOKEN){
		return _args;	
	}	
	switch(this->__current_token){
		case REG_TOKEN:
			dst = (RegAST*) this->reg_parser();break;
		case MEMSIZE_TOKEN: //working on getting this done
			dst = (MemRefAST*) this->memref_parser(this->identity_string);break;
		case MEMREF_TOKEN:
			dst = (MemRefAST*) this->memref_parser("QWORD");break; //assuming QWORD is the default
		
	}	

		
	//this->__log_format("Parser::op_arg_parser_2","[i] parsing second operand...");	
	if (this->__current_char == ']'){
		this->next_char();
	}
	this->next_char();
	this->lex_token();
	switch(this->__current_token){
		case REG_TOKEN:
			src = (RegAST*) this->reg_parser();break;			
		case NUMERIC_TOKEN:
			src = (NumericAST*) this->numeric_parser(false);break;	
		case MEMSIZE_TOKEN:
			src = (MemRefAST*) this->memref_parser(this->identity_string);break;
		case MEMREF_TOKEN:
			src = (MemRefAST*) this->memref_parser("QWORD");break;
	}
	_args.push_back(src);
	_args.push_back(dst);
	return _args;
}
BaseAST* Parser::adc_op_parser(void){ 
	//https://x86.puri.sm/html/file_module_x86_id_4.html
	/*
		Adds the destination operand (first operand), the source operand 
		(second operand), and the carry (CF) flag and stores the result in 
		the destination operand. The destination operand can be a register or a 
		memory location; the source operand can be an immediate, a register, or 
		a memory location. (However, two memory operands cannot be used in one 
		instruction.) The state of the CF flag represents a carry from a previous 
		addition. When an immediate value is used as an operand, it is sign-extended 
		to the length of the destination operand format.

		The ADC instruction does not distinguish between signed or unsigned operands. 
		Instead, the processor evaluates the result for both data types and sets the OF 
		and CF flags to indicate a carry in the signed or unsigned result, respectively. 
		The SF flag indicates the sign of the signed result.

		The ADC instruction is usually executed as part of a multibyte or multiword 
		addition in which an ADD instruction is followed by an ADC instruction.

		This instruction can be used with a LOCK prefix to allow the instruction 
		to be executed atomically.
	*/
	this->__log_format("LexerState::adc_op_parser()","[i] parsing adc op...");
	//should we sign extend the src before adding? need check on bitwise adc op	
	return this->add_op_parser();
}

BaseAST* Parser::addr32_op_parser(void){ this->__log_format("LexerState::addr32_op_parser()","[i] parsing addr32 op..."); return NULL;}

BaseAST* Parser::add_op_parser(void){ 
	/*
		Adds the first operand (destination operand) and the second operand (source operand) 
		and stores the result in the destination operand. The destination operand can be a register 
		or a memory location; the source operand can be an immediate, a register, or a memory location. 
		(However, two memory operands cannot be used in one instruction.) When an immediate value is 
		used as an operand, it is sign-extended to the length of the destination operand format.

		The ADD instruction performs integer addition. It evaluates the result for both signed and 
		unsigned integer operands and sets the OF and CF flags to indicate a carry (overflow) in the 
		signed or unsigned result, respectively. The SF flag indicates the sign of the signed result.

		This instruction can be used with a LOCK prefix to allow the instruction to be executed atomically.
	*/
	this->__log_format("LexerState::add_op_parser()","[i] parsing add op..."); 
	AddAST* _add;
	std::vector<BaseAST*> args = this->op_arg_parser_2();
	if (args.size() != 2){
		return NULL;
	}
	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_add = new AddAST(src,dst);

	this->__log_format("LexerState::add_op_parser()",_add->dump_str().c_str()); 
	return _add;

	
}

CallAST* Parser::call_op_parser(void){ 
	this->__log_format("LexerState::call_op_parser()","[i] parsing call op...");
	CallAST* _call;
	BaseAST* arg = this->op_arg_parser_1();
	_call = new CallAST(arg);

	this->__log_format("LexerState::mov_op_parser()",((CallAST*) _call)->dump_str().c_str()); 
	return _call;
	
}

CdqAST* Parser::cdq_op_parser(void){
	/*
		Doubles the size of the operand in register AX or EAX (depending on the operand size) 
		by means of sign extension and stores the result in registers DX:AX or EDX:EAX, respectively.

		The CWD instruction copies the sign (bit 15) of the value in the AX register into every 
		bit position in the DX register (see Figure 7-6 in the IA-32 Intel Architecture Software 
		Developer's Manual, Volume 1). The CDQ instruction copies the sign (bit 31) of the value 
		in the EAX register into every bit position in the EDX register.

		The CWD instruction can be used to produce a doubleword dividend from a word before a word
		division, and the CDQ instruction can be used to produce a quadword dividend from a doubleword 
		before doubleword division.

		The CWD and CDQ mnemonics reference the same opcode. The CWD instruction is intended for 
		use when the operand-size attribute is 16 and the CDQ instruction for when the operand-size 
		attribute is 32. Some assemblers may force the operand size to 16 when CWD is used and to 32 
		when CDQ is used. Others may treat these mnemonics as synonyms (CWD/CDQ) and use the current 
		setting of the operand-size attribute to determine the size of values to be converted, 
		regardless of the mnemonic used.
	*/
	/*
	if(OperandSize == 16) DX = SignExtend(AX); //CWD instruction
	else EDX = SignExtend(EAX); //OperandSize = 32, CDQ instruction */
	//TODO: fix when sign extension works properly
	return NULL;	

}

CdqeAST* Parser::cdqe_op_parser(void){ 
		
	this->__log_format("LexerState::cdqe_op_parser()","[i] parsing cdqe op...");
	return new CdqeAST();
}

CwdeAST* Parser::cwde_op_parser(void){ 
	this->__log_format("LexerState::cwde_op_parser()","[i] parsing cwde op...");
	return new CwdeAST();
}

CbwAST* Parser::cbw_op_parser(void){ 
	this->__log_format("LexerState::cbw_op_parser()","[i] parsing cbw op...");
	return new CbwAST();
}




BaseAST* Parser::clc_op_parser(void){ 
	this->__log_format("LexerState::clc_op_parser()","[i] parsing clc op..."); 
	return new ClcAST();
}

BaseAST* Parser::cld_op_parser(void){ this->__log_format("LexerState::cld_op_parser()","[i] parsing cld op..."); return NULL;}
BaseAST* Parser::cli_op_parser(void){ this->__log_format("LexerState::cli_op_parser()","[i] parsing cli op..."); return NULL;}
BaseAST* Parser::cmc_op_parser(void){ this->__log_format("LexerState::cmc_op_parser()","[i] parsing cmc op..."); return NULL;}
BaseAST* Parser::cmovae_op_parser(void){ this->__log_format("LexerState::cmovae_op_parser()","[i] parsing cmovae op..."); return NULL;}
BaseAST* Parser::cmova_op_parser(void){ this->__log_format("LexerState::cmova_op_parser()","[i] parsing cmova op..."); return NULL;}
BaseAST* Parser::cmovbe_op_parser(void){ this->__log_format("LexerState::cmovbe_op_parser()","[i] parsing cmovbe op..."); return NULL;}
BaseAST* Parser::cmovb_op_parser(void){ this->__log_format("LexerState::cmovb_op_parser()","[i] parsing cmovb op..."); return NULL;}
BaseAST* Parser::cmove_op_parser(void){ this->__log_format("LexerState::cmove_op_parser()","[i] parsing cmove op..."); return NULL;}
BaseAST* Parser::cmovge_op_parser(void){ this->__log_format("LexerState::cmovge_op_parser()","[i] parsing cmovge op..."); return NULL;}
BaseAST* Parser::cmovg_op_parser(void){ this->__log_format("LexerState::cmovg_op_parser()","[i] parsing cmovg op..."); return NULL;}
BaseAST* Parser::cmovle_op_parser(void){ this->__log_format("LexerState::cmovle_op_parser()","[i] parsing cmovle op..."); return NULL;}
BaseAST* Parser::cmovl_op_parser(void){ this->__log_format("LexerState::cmovl_op_parser()","[i] parsing cmovl op..."); return NULL;}
BaseAST* Parser::cmovne_op_parser(void){ this->__log_format("LexerState::cmovne_op_parser()","[i] parsing cmovne op..."); return NULL;}
BaseAST* Parser::cmovns_op_parser(void){ this->__log_format("LexerState::cmovns_op_parser()","[i] parsing cmovns op..."); return NULL;}
BaseAST* Parser::cmovs_op_parser(void){ this->__log_format("LexerState::cmovs_op_parser()","[i] parsing cmovs op..."); return NULL;}

CmpsAST* Parser::cmps_op_parser(void){ 
	this->__log_format("LexerState::cmps_op_parser()","[i] parsing cmps op...");
		
	CmpsAST* _cmps;
	std::vector<BaseAST*> args = this->op_arg_parser_2();
	if (args.size() != 2){
		return NULL;
	}
	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_cmps = new CmpsAST(src,dst);

	this->__log_format("LexerState::cmps_op_parser()",_cmps->dump_str().c_str()); 
	return _cmps;

}

CmpAST* Parser::cmp_op_parser(void){ 
	this->__log_format("LexerState::cmp_op_parser()","[i] parsing cmp op..."); 
	CmpAST* _cmp;
	std::vector<BaseAST*> args = this->op_arg_parser_2();

	if (args.size() != 2){
		return NULL;
	}

	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_cmp = new CmpAST(src,dst);

	this->__log_format("LexerState::cmp_op_parser()",_cmp->dump_str().c_str()); 
	return _cmp;

	
}

CmpsbAST* Parser::cmpsb_op_parser(void){ 
	this->__log_format("LexerState::cmpsb_op_parser()","[i] parsing cmpsb op..."); 
	CmpsbAST* _cmpsb;
	std::vector<BaseAST*> args = this->op_arg_parser_2();

	if (args.size() != 2){
		return NULL;
	}

	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_cmpsb = new CmpsbAST(src,dst);

	this->__log_format("LexerState::cmpsb_op_parser()",_cmpsb->dump_str().c_str()); 
	return _cmpsb;

	
}

CmpswAST* Parser::cmpsw_op_parser(void){ 
	this->__log_format("LexerState::cmpsw_op_parser()","[i] parsing cmpsw op..."); 
	CmpswAST* _cmpsw;
	std::vector<BaseAST*> args = this->op_arg_parser_2();

	if (args.size() != 2){
		return NULL;
	}

	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_cmpsw = new CmpswAST(src,dst);

	this->__log_format("LexerState::cmpsw_op_parser()",_cmpsw->dump_str().c_str()); 
	return _cmpsw;

	
}

CmpsdAST* Parser::cmpsd_op_parser(void){ 
	this->__log_format("LexerState::cmpsd_op_parser()","[i] parsing cmpsd op..."); 
	CmpsdAST* _cmpsd;
	std::vector<BaseAST*> args = this->op_arg_parser_2();

	if (args.size() != 2){
		return NULL;
	}

	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_cmpsd = new CmpsdAST(src,dst);

	this->__log_format("LexerState::cmpsd_op_parser()",_cmpsd->dump_str().c_str()); 
	return _cmpsd;

	
}
CmpsqAST* Parser::cmpsq_op_parser(void){ 
	this->__log_format("LexerState::cmpsq_op_parser()","[i] parsing cmpsq op..."); 
	CmpsqAST* _cmpsq;
	std::vector<BaseAST*> args = this->op_arg_parser_2();

	if (args.size() != 2){
		return NULL;
	}

	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_cmpsq = new CmpsqAST(src,dst);

	this->__log_format("LexerState::cmpsq_op_parser()",_cmpsq->dump_str().c_str()); 
	return _cmpsq;

	
}

BaseAST* Parser::cqo_op_parser(void){ this->__log_format("LexerState::cqo_op_parser()","[i] parsing cqo op..."); return NULL;}
BaseAST* Parser::dec_op_parser(void){ this->__log_format("LexerState::dec_op_parser()","[i] parsing dec op..."); return NULL;}
BaseAST* Parser::div_op_parser(void){ this->__log_format("LexerState::div_op_parser()","[i] parsing div op..."); return NULL;}

BaseAST* Parser::fcomip_op_parser(void){ this->__log_format("LexerState::fcomip_op_parser()","[i] parsing fcomip op..."); return NULL;}
BaseAST* Parser::fcomp_op_parser(void){ this->__log_format("LexerState::fcomp_op_parser()","[i] parsing fcomp op..."); return NULL;}
BaseAST* Parser::fcom_op_parser(void){ this->__log_format("LexerState::fcom_op_parser()","[i] parsing fcom op..."); return NULL;}
BaseAST* Parser::fdivp_op_parser(void){ this->__log_format("LexerState::fdivp_op_parser()","[i] parsing fdivp op..."); return NULL;}
BaseAST* Parser::fdivrp_op_parser(void){ this->__log_format("LexerState::fdivrp_op_parser()","[i] parsing fdivrp op..."); return NULL;}
BaseAST* Parser::fdivr_op_parser(void){ this->__log_format("LexerState::fdivr_op_parser()","[i] parsing fdivr op..."); return NULL;}
BaseAST* Parser::fdiv_op_parser(void){ this->__log_format("LexerState::fdiv_op_parser()","[i] parsing fdiv op..."); return NULL;}
BaseAST* Parser::fiadd_op_parser(void){ this->__log_format("LexerState::fiadd_op_parser()","[i] parsing fiadd op..."); return NULL;}
BaseAST* Parser::ficomp_op_parser(void){ this->__log_format("LexerState::ficomp_op_parser()","[i] parsing ficomp op..."); return NULL;}
BaseAST* Parser::fidivr_op_parser(void){ this->__log_format("LexerState::fidivr_op_parser()","[i] parsing fidivr op..."); return NULL;}
BaseAST* Parser::fidiv_op_parser(void){ this->__log_format("LexerState::fidiv_op_parser()","[i] parsing fidiv op..."); return NULL;}
BaseAST* Parser::fild_op_parser(void){ this->__log_format("LexerState::fild_op_parser()","[i] parsing fild op..."); return NULL;}
BaseAST* Parser::fimul_op_parser(void){ this->__log_format("LexerState::fimul_op_parser()","[i] parsing fimul op..."); return NULL;}
BaseAST* Parser::fisttp_op_parser(void){ this->__log_format("LexerState::fisttp_op_parser()","[i] parsing fisttp op..."); return NULL;}
BaseAST* Parser::fld_op_parser(void){ this->__log_format("LexerState::fld_op_parser()","[i] parsing fld op..."); return NULL;}
BaseAST* Parser::fldz_op_parser(void){ this->__log_format("LexerState::fldz_op_parser()","[i] parsing fldz op..."); return NULL;}
BaseAST* Parser::fmul_op_parser(void){ this->__log_format("LexerState::fmul_op_parser()","[i] parsing fmul op..."); return NULL;}
BaseAST* Parser::fprem_op_parser(void){ this->__log_format("LexerState::fprem_op_parser()","[i] parsing fprem op..."); return NULL;}
BaseAST* Parser::fprem1_op_parser(void){ this->__log_format("LexerState::fprem1_op_parser()","[i] parsing fprem1 op..."); return NULL;}
BaseAST* Parser::fscale_op_parser(void){ this->__log_format("LexerState::fscale_op_parser()","[i] parsing fscale op..."); return NULL;}
BaseAST* Parser::fsqrt_op_parser(void){ this->__log_format("LexerState::fsqrt_op_parser()","[i] parsing fsqrt op..."); return NULL;}
BaseAST* Parser::fstp_op_parser(void){ this->__log_format("LexerState::fstp_op_parser()","[i] parsing fstp op..."); return NULL;}
BaseAST* Parser::fsubp_op_parser(void){ this->__log_format("LexerState::fsubp_op_parser()","[i] parsing fsubp op..."); return NULL;}
BaseAST* Parser::fsubr_op_parser(void){ this->__log_format("LexerState::fsubr_op_parser()","[i] parsing fsubr op..."); return NULL;}
BaseAST* Parser::fsub_op_parser(void){ this->__log_format("LexerState::fsub_op_parser()","[i] parsing fsub op..."); return NULL;}
BaseAST* Parser::fs_op_parser(void){ this->__log_format("LexerState::fs_op_parser()","[i] parsing fs op..."); return NULL;}
BaseAST* Parser::fucompp_op_parser(void){ this->__log_format("LexerState::fucompp_op_parser()","[i] parsing fucompp op..."); return NULL;}
BaseAST* Parser::fwait_op_parser(void){ this->__log_format("LexerState::fwait_op_parser()","[i] parsing fwait op..."); return NULL;}
BaseAST* Parser::icebp_op_parser(void){ this->__log_format("LexerState::icebp_op_parser()","[i] parsing icebp op..."); return NULL;}
BaseAST* Parser::idiv_op_parser(void){ this->__log_format("LexerState::idiv_op_parser()","[i] parsing idiv op..."); return NULL;}
BaseAST* Parser::imul_op_parser(void){ this->__log_format("LexerState::imul_op_parser()","[i] parsing imul op..."); return NULL;}
BaseAST* Parser::inc_op_parser(void){ this->__log_format("LexerState::inc_op_parser()","[i] parsing inc op..."); return NULL;}
BaseAST* Parser::in_op_parser(void){ this->__log_format("LexerState::in_op_parser()","[i] parsing in op..."); return NULL;}
BaseAST* Parser::ins_op_parser(void){ this->__log_format("LexerState::ins_op_parser()","[i] parsing ins op..."); return NULL;}
BaseAST* Parser::int_op_parser(void){ this->__log_format("LexerState::int_op_parser()","[i] parsing int op..."); return NULL;}
BaseAST* Parser::int3_op_parser(void){ this->__log_format("LexerState::int3_op_parser()","[i] parsing int3 op..."); return NULL;}

JaAST* Parser::ja_op_parser(void){ 
	this->__log_format("LexerState::ja_op_parser()","[i] parsing ja op..."); 
	JaAST* _ja;
	BaseAST* arg = this->op_arg_parser_1();
	_ja = new JaAST(arg);

	this->__log_format("LexerState::ja_op_parser()",((JaAST*) _ja)->dump_str().c_str()); 
	return _ja;

}
JaeAST* Parser::jae_op_parser(void){ 
	this->__log_format("LexerState::jae_op_parser()","[i] parsing jae op...");
	JaeAST* _jae;
	BaseAST* arg = this->op_arg_parser_1();
	_jae = new JaeAST(arg);

	this->__log_format("LexerState::jae_op_parser()",((JaeAST*) _jae)->dump_str().c_str()); 
	return _jae;

}
JbAST* Parser::jb_op_parser(void){ 
	this->__log_format("LexerState::jb_op_parser()","[i] parsing jb op...");
	JbAST* _jb;
	BaseAST* arg = this->op_arg_parser_1();
	_jb = new JbAST(arg);

	this->__log_format("LexerState::jb_op_parser()",((JbAST*) _jb)->dump_str().c_str()); 
	return _jb;
}

JbeAST* Parser::jbe_op_parser(void){ 
	this->__log_format("LexerState::jbe_op_parser()","[i] parsing jbe op...");
	JbeAST* _jbe;
	BaseAST* arg = this->op_arg_parser_1();
	_jbe = new JbeAST(arg);

	this->__log_format("LexerState::jbe_op_parser()",((JbeAST*) _jbe)->dump_str().c_str()); 
	return _jbe;
}

JeAST* Parser::je_op_parser(void){ 
	this->__log_format("LexerState::je_op_parser()","[i] parsing je op..."); 
	JeAST* _je;
	BaseAST* arg = this->op_arg_parser_1();
	_je = new JeAST(arg);

	this->__log_format("LexerState::je_op_parser()",((JeAST*) _je)->dump_str().c_str()); 
	return _je;

}
JgeAST* Parser::jge_op_parser(void){ 
	this->__log_format("LexerState::jge_op_parser()","[i] parsing jge op..."); 
	JgeAST* _jge;
	BaseAST* arg = this->op_arg_parser_1();
	_jge = new JgeAST(arg);

	this->__log_format("LexerState::jge_op_parser()",((JgeAST*) _jge)->dump_str().c_str()); 
	return _jge;
}

JgAST* Parser::jg_op_parser(void){ 
	this->__log_format("LexerState::jg_op_parser()","[i] parsing jg op..."); 
	JgAST* _jg;
	BaseAST* arg = this->op_arg_parser_1();
	_jg = new JgAST(arg);

	this->__log_format("LexerState::jg_op_parser()",((JgeAST*) _jg)->dump_str().c_str()); 
	return _jg;
}

JlAST* Parser::jl_op_parser(void){ 
	this->__log_format("LexerState::jl_op_parser()","[i] parsing jl op..."); 
	JlAST* _jl;
	BaseAST* arg = this->op_arg_parser_1();
	_jl = new JlAST(arg);

	this->__log_format("LexerState::jl_op_parser()",((JlAST*) _jl)->dump_str().c_str()); 
	return _jl;
}

JleAST* Parser::jle_op_parser(void){ 
	this->__log_format("LexerState::jle_op_parser()","[i] parsing jle op..."); 
	JleAST* _jle;
	BaseAST* arg = this->op_arg_parser_1();
	_jle = new JleAST(arg);

	this->__log_format("LexerState::jle_op_parser()",((JleAST*) _jle)->dump_str().c_str()); 
	return _jle;

}

JmpAST* Parser::jmp_op_parser(void){ 
	this->__log_format("LexerState::jmp_op_parser()","[i] parsing jmp op..."); 
	JmpAST* _jmp;
	BaseAST* arg = this->op_arg_parser_1();
	_jmp = new JmpAST(arg);

	this->__log_format("LexerState::jmp_op_parser()",((JmpAST*) _jmp)->dump_str().c_str()); 
	return _jmp;

}

JneAST* Parser::jne_op_parser(void){ 
	this->__log_format("LexerState::jne_op_parser()","[i] parsing jne op...");
	JneAST* _jne;
	BaseAST* arg = this->op_arg_parser_1();
	_jne = new JneAST(arg);

	this->__log_format("LexerState::jne_op_parser()",((JneAST*) _jne)->dump_str().c_str()); 
	return _jne;

}

JnoAST* Parser::jno_op_parser(void){ 
	this->__log_format("LexerState::jno_op_parser()","[i] parsing jno op..."); 
	JnoAST* _jno;
	BaseAST* arg = this->op_arg_parser_1();
	_jno = new JnoAST(arg);

	this->__log_format("LexerState::jno_op_parser()",((JnoAST*) _jno)->dump_str().c_str()); 
	return _jno;

	
}
JnpAST* Parser::jnp_op_parser(void){ 
	this->__log_format("LexerState::jnp_op_parser()","[i] parsing jnp op..."); 
	JnpAST* _jnp;
	BaseAST* arg = this->op_arg_parser_1();
	_jnp = new JnpAST(arg);

	this->__log_format("lexerstate::jnp_op_parser()",((JnpAST*) _jnp)->dump_str().c_str()); 
	return _jnp;

}
JnsAST* Parser::jns_op_parser(void){ 
	this->__log_format("LexerState::jns_op_parser()","[i] parsing jns op..."); 
	JnsAST* _jns;
	BaseAST* arg = this->op_arg_parser_1();
	_jns = new JnsAST(arg);

	this->__log_format("LexerState::jns_op_parser()",((JnsAST*) _jns)->dump_str().c_str()); 
	return _jns;

}
JoAST* Parser::jo_op_parser(void){ 
	this->__log_format("LexerState::jo_op_parser()","[i] parsing jo op..."); 
	JoAST* _jo;
	BaseAST* arg = this->op_arg_parser_1();
	_jo = new JoAST(arg);

	this->__log_format("LexerState::jo_op_parser()",((JoAST*) _jo)->dump_str().c_str()); 
	return _jo;

}
JpAST* Parser::jp_op_parser(void){ 
	this->__log_format("LexerState::jp_op_parser()","[i] parsing jp op...");
	JpAST* _jp;
	BaseAST* arg = this->op_arg_parser_1();
	_jp = new JpAST(arg);

	this->__log_format("LexerState::jp_op_parser()",((JpAST*) _jp)->dump_str().c_str()); 
	return _jp;

}

JsAST* Parser::js_op_parser(void){ 
	this->__log_format("LexerState::js_op_parser()","[i] parsing js op..."); 
	JsAST* _js;
	BaseAST* arg = this->op_arg_parser_1();
	_js = new JsAST(arg);

	this->__log_format("LexerState::jp_op_parser()",((JsAST*) _js)->dump_str().c_str()); 
	return _js;
}

BaseAST* Parser::jrcxz_op_parser(void){ this->__log_format("LexerState::jrcxz_op_parser()","[i] parsing jrcxz op..."); return NULL;}
BaseAST* Parser::lahf_op_parser(void){ this->__log_format("LexerState::lahf_op_parser()","[i] parsing lahf op..."); return NULL;}
BaseAST* Parser::lea_op_parser(void){ 

	/*
	
	leaAST[ 
		regAST[ 'rsi']  
		memRef[  QWORD 
			binOpAST[{+}  
				regAST[ 'rip']  
				numAST[ '0x149d7']
			]
		]
	]

	*/
	this->__log_format("LexerState::lea_op_parser()","[i] parsing lea op..."); 
	LeaAST* _lea_op;
	std::vector<BaseAST*> _args = this->op_arg_parser_2();
	_lea_op = new LeaAST(_args[0],_args[1]);

	this->__log_format("LexerState::lea_op_parser()",((LeaAST*) _lea_op)->dump_str().c_str()); 
	return _lea_op;
	
}
BaseAST* Parser::leave_op_parser(void){ this->__log_format("LexerState::leave_op_parser()","[i] parsing leave op..."); return NULL;}
BaseAST* Parser::lock_op_parser(void){ this->__log_format("LexerState::lock_op_parser()","[i] parsing lock op..."); return NULL;}
BaseAST* Parser::lods_op_parser(void){ this->__log_format("LexerState::lods_op_parser()","[i] parsing lods op..."); return NULL;}
BaseAST* Parser::loop_op_parser(void){ this->__log_format("LexerState::loop_op_parser()","[i] parsing loop op..."); return NULL;}
BaseAST* Parser::loope_op_parser(void){ this->__log_format("LexerState::loope_op_parser()","[i] parsing loope op..."); return NULL;}
BaseAST* Parser::loopne_op_parser(void){ this->__log_format("LexerState::loopne_op_parser()","[i] parsing loopne op..."); return NULL;}
BaseAST* Parser::lsl_op_parser(void){ this->__log_format("LexerState::lsl_op_parser()","[i] parsing lsl op..."); return NULL;}

BaseAST* Parser::maskmovq_op_parser(void){ this->__log_format("LexerState::maskmovq_op_parser()","[i] parsing maskmovq op..."); return NULL;}

MovAST* Parser::movabs_op_parser(void){ 
	this->__log_format("LexerState::movabs_op_parser()","[i] parsing movabs op...");
	return this->mov_op_parser(); //GAS specific mov apparently 
}
MovAST* Parser::movaps_op_parser(void){ 
	this->__log_format("LexerState::movaps_op_parser()","[i] parsing movaps op..."); 
	return this->mov_op_parser();
	
}
MovAST* Parser::movdqa_op_parser(void){ 
	this->__log_format("LexerState::movdqa_op_parser()","[i] parsing movdqa op..."); 
	return this->mov_op_parser();
}

MovAST* Parser::movdqu_op_parser(void){ 
	this->__log_format("LexerState::movdqu_op_parser()","[i] parsing movdqu op..."); 
	return this->mov_op_parser();
}
MovAST* Parser::movs_op_parser(void){ 
	this->__log_format("LexerState::movs_op_parser()","[i] parsing movs op..."); 
	return this->mov_op_parser();
}
MovAST* Parser::movsx_op_parser(void){ 
	this->__log_format("LexerState::movsx_op_parser()","[i] parsing movsx op..."); 
	MovAST* _mov = this->mov_op_parser();
	BaseAST* _src = _mov->get_src();
	_mov->set_src(this->sign_extend(true,_src));	 
	return _mov;  
}
MovAST* Parser::movsxd_op_parser(void){ 
	this->__log_format("LexerState::movsxd_op_parser()","[i] parsing movsxd op..."); 
	return this->movsx_op_parser();
}

MovAST* Parser::movups_op_parser(void){ 
	this->__log_format("LexerState::movups_op_parser()","[i] parsing movups op..."); 
	return this->mov_op_parser();
}


MovAST* Parser::mov_op_parser(void){ 
	this->__log_format("Parser::mov_op_parser","[i] parsing mov op...");
	MovAST* _mov;
	std::vector<BaseAST*> args = this->op_arg_parser_2();
	if (args.size() != 2){
		return NULL;
	}
	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_mov = new MovAST(src,dst);

	this->__log_format("LexerState::mov_op_parser()",_mov->dump_str().c_str()); 
	return _mov;
}

MovAST* Parser::movzx_op_parser(void){ 
	this->__log_format("LexerState::movzx_op_parser()","[i] parsing movzx op..."); 
	//TODO: when get_size is fixed implement the zero extend here
	return this->mov_op_parser();
}
MulAST* Parser::mul_op_parser(void){ 
	this->__log_format("LexerState::mul_op_parser()","[i] parsing mul op..."); 
	MulAST* _mul;
	BaseAST* arg = this->op_arg_parser_1();
	_mul = new MulAST(arg);
	this->__log_format("LexerState::mul_op_parser()",_mul->dump_str().c_str()); 
	return _mul;
}
NegAST* Parser::neg_op_parser(void){ 
	this->__log_format("LexerState::neg_op_parser()","[i] parsing neg op..."); 
	BaseAST* arg = this->op_arg_parser_1();
	NegAST* _neg = new NegAST(arg);
	this->__log_format("LexerState::neg_op_parser()",_neg->dump_str().c_str()); 
	return _neg;
}

NopAST* Parser::nop_op_parser(void){ 
	//nop actually takes two arguments
	this->__log_format("LexerState::nop_op_parser()","[i] parsing nop op..."); 
	NopAST* _nop;
	std::vector<BaseAST*> args = this->op_arg_parser_2();
	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_nop = new NopAST(src,dst);

	this->__log_format("LexerState::not_op_parser()",_nop->dump_str().c_str()); 
	return _nop;

}

NotAST* Parser::not_op_parser(void){ 
	this->__log_format("LexerState::not_op_parser()","[i] parsing not op..."); 
	return NULL;
}
BaseAST* Parser::or_op_parser(void){ this->__log_format("LexerState::or_op_parser()","[i] parsing or op..."); return NULL;}
BaseAST* Parser::out_op_parser(void){ this->__log_format("LexerState::out_op_parser()","[i] parsing out op..."); return NULL;}
BaseAST* Parser::outs_op_parser(void){ this->__log_format("LexerState::outs_op_parser()","[i] parsing outs op..."); return NULL;}
BaseAST* Parser::paddb_op_parser(void){ this->__log_format("LexerState::paddb_op_parser()","[i] parsing paddb op..."); return NULL;}
BaseAST* Parser::popf_op_parser(void){ this->__log_format("LexerState::popf_op_parser()","[i] parsing popf op..."); return NULL;}
BaseAST* Parser::pop_op_parser(void){ this->__log_format("LexerState::pop_op_parser()","[i] parsing pop op..."); return NULL;}
BaseAST* Parser::psadbw_op_parser(void){ this->__log_format("LexerState::psadbw_op_parser()","[i] parsing psadbw op..."); return NULL;}
BaseAST* Parser::psubb_op_parser(void){ this->__log_format("LexerState::psubb_op_parser()","[i] parsing psubb op..."); return NULL;}
BaseAST* Parser::psubd_op_parser(void){ this->__log_format("LexerState::psubd_op_parser()","[i] parsing psubd op..."); return NULL;}
BaseAST* Parser::psubw_op_parser(void){ this->__log_format("LexerState::psubw_op_parser()","[i] parsing psubw op..."); return NULL;}
BaseAST* Parser::pushf_op_parser(void){ this->__log_format("LexerState::pushf_op_parser()","[i] parsing pushf op..."); return NULL;}

PushAST* Parser::push_op_parser(void){ 
	PushAST* _push;
	RegAST* _reg;
	
	//this->__log_format("LexerState::push_op_parser()","[i] parsing push op..."); 
	this->lex_token(); //should hold the register in the identity string now
		
	if (this->__current_token != REG_TOKEN){
		return NULL;// bad push op with no register specified, bail
	}

	_reg = this->reg_parser();
	_push = new PushAST(_reg);		
	this->__log_format("LexerState::push_op_parser()",_push->dump_str().c_str()); 
	return _push;
}

BaseAST* Parser::pxor_op_parser(void){ this->__log_format("LexerState::pxor_op_parser()","[i] parsing pxor op..."); return NULL;}
BaseAST* Parser::rcl_op_parser(void){ this->__log_format("LexerState::rcl_op_parser()","[i] parsing rcl op..."); return NULL;}
BaseAST* Parser::rcr_op_parser(void){ this->__log_format("LexerState::rcr_op_parser()","[i] parsing rcr op..."); return NULL;}
BaseAST* Parser::repnz_op_parser(void){ this->__log_format("LexerState::repnz_op_parser()","[i] parsing repnz op..."); return NULL;}
BaseAST* Parser::rep_op_parser(void){ this->__log_format("LexerState::rep_op_parser()","[i] parsing rep op..."); return NULL;}
BaseAST* Parser::repz_op_parser(void){ this->__log_format("LexerState::repz_op_parser()","[i] parsing repz op..."); return NULL;}
BaseAST* Parser::ret_op_parser(void){ this->__log_format("LexerState::ret_op_parser()","[i] parsing ret op..."); return NULL;}
BaseAST* Parser::retf_op_parser(void){ this->__log_format("LexerState::retf_op_parser()","[i] parsing retf op..."); return NULL;}
BaseAST* Parser::rex_op_parser(void){ this->__log_format("LexerState::rex_op_parser()","[i] parsing rex op..."); return NULL;}
BaseAST* Parser::rol_op_parser(void){ this->__log_format("LexerState::rol_op_parser()","[i] parsing rol op..."); return NULL;}
BaseAST* Parser::ror_op_parser(void){ this->__log_format("LexerState::ror_op_parser()","[i] parsing ror op..."); return NULL;}
BaseAST* Parser::sahf_op_parser(void){ this->__log_format("LexerState::sahf_op_parser()","[i] parsing sahf op..."); return NULL;}
BaseAST* Parser::sar_op_parser(void){ this->__log_format("LexerState::sar_op_parser()","[i] parsing sar op..."); return NULL;}
BaseAST* Parser::sbb_op_parser(void){ this->__log_format("LexerState::sbb_op_parser()","[i] parsing sbb op..."); return NULL;}
BaseAST* Parser::scas_op_parser(void){ this->__log_format("LexerState::scas_op_parser()","[i] parsing scas op..."); return NULL;}
BaseAST* Parser::seta_op_parser(void){ this->__log_format("LexerState::seta_op_parser()","[i] parsing seta op..."); return NULL;}
BaseAST* Parser::setbe_op_parser(void){ this->__log_format("LexerState::setbe_op_parser()","[i] parsing setbe op..."); return NULL;}
BaseAST* Parser::setb_op_parser(void){ this->__log_format("LexerState::setb_op_parser()","[i] parsing setb op..."); return NULL;}
BaseAST* Parser::sete_op_parser(void){ this->__log_format("LexerState::sete_op_parser()","[i] parsing sete op..."); return NULL;}
BaseAST* Parser::setge_op_parser(void){ this->__log_format("LexerState::setge_op_parser()","[i] parsing setge op..."); return NULL;}
BaseAST* Parser::setg_op_parser(void){ this->__log_format("LexerState::setg_op_parser()","[i] parsing setg op..."); return NULL;}
BaseAST* Parser::setl_op_parser(void){ this->__log_format("LexerState::setl_op_parser()","[i] parsing setl op..."); return NULL;}
BaseAST* Parser::setle_op_parser(void){ this->__log_format("LexerState::setle_op_parser()","[i] parsing setle op..."); return NULL;}
BaseAST* Parser::setne_op_parser(void){ this->__log_format("LexerState::setne_op_parser()","[i] parsing setne op..."); return NULL;}
BaseAST* Parser::sgdt_op_parser(void){ this->__log_format("LexerState::sgdt_op_parser()","[i] parsing sgdt op..."); return NULL;}
BaseAST* Parser::shl_op_parser(void){ this->__log_format("LexerState::shl_op_parser()","[i] parsing shl op..."); return NULL;}
BaseAST* Parser::shr_op_parser(void){ this->__log_format("LexerState::shr_op_parser()","[i] parsing shr op..."); return NULL;}
BaseAST* Parser::sidt_op_parser(void){ this->__log_format("LexerState::sidt_op_parser()","[i] parsing sidt op..."); return NULL;}
BaseAST* Parser::sldt_op_parser(void){ this->__log_format("LexerState::sldt_op_parser()","[i] parsing sldt op..."); return NULL;}
BaseAST* Parser::ss_op_parser(void){ this->__log_format("LexerState::ss_op_parser()","[i] parsing ss op..."); return NULL;}
BaseAST* Parser::stc_op_parser(void){ this->__log_format("LexerState::stc_op_parser()","[i] parsing stc op..."); return NULL;}
BaseAST* Parser::std_op_parser(void){ this->__log_format("LexerState::std_op_parser()","[i] parsing std op..."); return NULL;}
BaseAST* Parser::sti_op_parser(void){ this->__log_format("LexerState::sti_op_parser()","[i] parsing sti op..."); return NULL;}
BaseAST* Parser::stos_op_parser(void){ this->__log_format("LexerState::stos_op_parser()","[i] parsing stos op..."); return NULL;}
BaseAST* Parser::str_op_parser(void){ this->__log_format("LexerState::str_op_parser()","[i] parsing str op..."); return NULL;}

BaseAST* Parser::sub_op_parser(void){ 
	this->__log_format("LexerState::sub_op_parser()","[i] parsing sub op...");
	SubAST* _sub_op;
	std::vector<BaseAST*> _args = this->op_arg_parser_2();
	_sub_op = new SubAST(_args[0],_args[1]);
	this->__log_format("LexerState::sub_op_parser()",_sub_op->dump_str().c_str());
	return _sub_op;

}

BaseAST* Parser::syscall_op_parser(void){ this->__log_format("LexerState::syscall_op_parser()","[i] parsing syscall op..."); return NULL;}
BaseAST* Parser::xchg_op_parser(void){ this->__log_format("LexerState::xchg_op_parser()","[i] parsing xchg op..."); return NULL;}
BaseAST* Parser::xlat_op_parser(void){ this->__log_format("LexerState::xlat_op_parser()","[i] parsing xlat op..."); return NULL;}

XorAST* Parser::xor_op_parser(void){ 
	this->__log_format("LexerState::xor_op_parser()","[i] parsing xor op..."); 
	XorAST* _xor;
	std::vector<BaseAST*> args = this->op_arg_parser_2();

	if (args.size() != 2){
		return NULL;
	}
	BaseAST* src = args[0];
	BaseAST* dst = args[1];

	_xor = new XorAST(src,dst);

	this->__log_format("LexerState::mov_op_parser()",_xor->dump_str().c_str()); 
	return _xor;

}



void LexerState::__init_segreg_names(){
	this->segreg_names.push_back("ss");	
	this->segreg_names.push_back("cs");	
	this->segreg_names.push_back("es");	
	this->segreg_names.push_back("ds");	
	this->segreg_names.push_back("fs");	
	this->segreg_names.push_back("gs");	
}
unsigned int LexerState::get_reg(RegAST* reg){
	std::string name = reg->get_name();
	if (name == "rax"){ return 	rax_reg; }
	if (name == "rbx"){ return 	rbx_reg; }
	if (name == "rcx"){ return 	rcx_reg; }
	if (name == "rdx"){ return 	rdx_reg; }
	if (name == "rbp"){ return 	rbp_reg; }
	if (name == "rsp"){ return 	rsp_reg; }
	if (name == "rip"){ return 	rip_reg; }
	if (name == "rdi"){ return 	rdi_reg; }
	if (name == "rsi"){ return 	rsi_reg; }
	if (name == "r8"){ return  	r8_reg; }
	if (name == "r9"){ return  	r9_reg; }
	if (name == "r10"){ return 	r10_reg; }
	if (name == "r11"){ return 	r11_reg; }
	if (name == "r12"){ return 	r12_reg; }
	if (name == "r12d"){ return r12d_reg; }
	if (name == "r13"){ return 	r13_reg; }
	if (name == "r14"){ return 	r14_reg; }
	if (name == "r15"){ return 	r15_reg; }
	if (name == "eax"){ return 	eax_reg; }
	if (name == "ebx"){ return 	ebx_reg; }
	if (name == "ecx"){ return 	ecx_reg; }
	if (name == "edx"){ return 	edx_reg; }
	if (name == "edi"){ return 	edi_reg; }
	if (name == "esi"){ return 	esi_reg; }
	return -1;
}

unsigned short LexerState::get_size(BaseAST *arg){
	std::string log_string;
	this->__log_format("LexerState::get_size","determining size of operand...");

	if (dynamic_cast<RegAST*>(arg) != nullptr){ //TODO: fix this it crashes, why does this sometimes crash??
		RegAST* _reg = (RegAST*) arg;	
		std::string reg_name = _reg->get_name();
		log_string = "reg operand name ->" + reg_name; 	
		this->__log_format("LexerState::get_size",log_string.c_str());
		if (reg_name.size() == 3){
			switch(reg_name[0]){
				case 'r': return 64;
				case 'e': return 32;
			}
		}
		else if (reg_name.size() == 2){
			return 16;
		}
	}else if(typeid(NumericAST*) == typeid(arg)){
		std::string _value = ((NumericAST*) arg)->get_value();
		return _value.size()*16;
	}else if (typeid(SegRegAST*) == typeid(arg)){
		return 64;
	}
	else if(typeid(MemRefAST*) == typeid(arg)){
		std::string _size = ((MemRefAST* )arg)->get_size_type();
		if (_size == "BYTE"){
			return 8;
		}else if (_size == "WORD"){
			return 16;
		}else if (_size == "DWORD"){
			return 32;
		}
		return 64; //TODO: implement memref checker here.
	}
	return 64;
}
BaseAST* Parser::sign_extend(bool is_signed, BaseAST *arg){
//https://stackoverflow.com/questions/15729765/sign-extension-with-bitwise-shift-operation

	NumericAST* shift_base;
	NumericAST* or_base;
	this->__log_format("Parser::sign_extend","sign extending arg...");

	//if (dynamic_cast<RegAST*>(arg) != nullptr){
		switch(this->get_size(arg)){
			case 16: 
				shift_base = new NumericAST("16");
				this->__log_format("Parser::sign_extend","size of arg 16");
				break;
			case 32: 
				shift_base = new NumericAST("32");
				this->__log_format("Parser::sign_extend","size of arg 32");
				break;
			case 64:
				this->__log_format("Parser::sign_extend","size of arg 64");
				return arg; 
		}
		if (!is_signed){
			return new ShlAST(arg, shift_base);
		}
		else{
			switch(this->get_size(arg)){
				case 16: or_base = new NumericAST("ffff"); break;
				case 32: or_base = new NumericAST("ffffffff");break;
			}
			ShlAST* _shl = new ShlAST(arg,shift_base);
			return new OrAST(_shl,or_base);

		}	
	//}	
	return arg;
}
void LexerState::__init_reg_names(){
	this->reg_names.push_back("rax");	
	this->reg_names.push_back("rbx");	
	this->reg_names.push_back("rcx");	
	this->reg_names.push_back("rdx");	
	this->reg_names.push_back("rbp");	
	this->reg_names.push_back("rsp");	
	this->reg_names.push_back("rip");	
	this->reg_names.push_back("rdi");	
	this->reg_names.push_back("rsi");	
	this->reg_names.push_back("r8");	
	this->reg_names.push_back("r9");	
	this->reg_names.push_back("r10");	
	this->reg_names.push_back("r11");	
	this->reg_names.push_back("r12");	
	this->reg_names.push_back("r12d");	
	this->reg_names.push_back("r13");	
	this->reg_names.push_back("r14");	
	this->reg_names.push_back("r15");	
	this->reg_names.push_back("eax");	
	this->reg_names.push_back("ebx");	
	this->reg_names.push_back("ecx");	
	this->reg_names.push_back("edx");	
	this->reg_names.push_back("edi");	
	this->reg_names.push_back("esi");	
}
void LexerState::__init_op_names(){
	this->__log_format("LexerState::init_op_names()","initializing op names...");
	this->op_names.push_back("adc");
	this->op_names.push_back("add");
	this->op_names.push_back("addr32");
	this->op_names.push_back("add");
	this->op_names.push_back("cdqe");
	this->op_names.push_back("call");
	this->op_names.push_back("clc");
	this->op_names.push_back("cld");
	this->op_names.push_back("cli");
	this->op_names.push_back("cmc");
	this->op_names.push_back("cmova");
	this->op_names.push_back("cmovae");
	this->op_names.push_back("cmova");
	this->op_names.push_back("cmovbe");
	this->op_names.push_back("cmovb");
	this->op_names.push_back("cmove");
	this->op_names.push_back("cmovg");
	this->op_names.push_back("cmovge");
	this->op_names.push_back("cmovg");
	this->op_names.push_back("cmovl");
	this->op_names.push_back("cmovle");
	this->op_names.push_back("cmovl");
	this->op_names.push_back("cmovne");
	this->op_names.push_back("cmovns");
	this->op_names.push_back("cmovs");
	this->op_names.push_back("cmp");
	this->op_names.push_back("cmps");
	this->op_names.push_back("cmp");
	this->op_names.push_back("cqo");
	this->op_names.push_back("cwde");
	this->op_names.push_back("dec");
	this->op_names.push_back("div");
	this->op_names.push_back("fcom");
	this->op_names.push_back("fcomip");
	this->op_names.push_back("fcomp");
	this->op_names.push_back("fcom");
	this->op_names.push_back("fdiv");
	this->op_names.push_back("fdivp");
	this->op_names.push_back("fdiv");
	this->op_names.push_back("fdivr");
	this->op_names.push_back("fdivrp");
	this->op_names.push_back("fdivr");
	this->op_names.push_back("fdiv");
	this->op_names.push_back("fiadd");
	this->op_names.push_back("ficomp");
	this->op_names.push_back("fidivr");
	this->op_names.push_back("fidiv");
	this->op_names.push_back("fild");
	this->op_names.push_back("fimul");
	this->op_names.push_back("fisttp");
	this->op_names.push_back("fld");
	this->op_names.push_back("fldz");
	this->op_names.push_back("fmul");
	this->op_names.push_back("fprem");
	this->op_names.push_back("fprem1");
	this->op_names.push_back("fscale");
	this->op_names.push_back("fsqrt");
	this->op_names.push_back("fstp");
	this->op_names.push_back("fsub");
	this->op_names.push_back("fsubp");
	this->op_names.push_back("fsub");
	this->op_names.push_back("fsubr");
	this->op_names.push_back("fsub");
	this->op_names.push_back("fucompp");
	this->op_names.push_back("fwait");
	this->op_names.push_back("icebp");
	this->op_names.push_back("idiv");
	this->op_names.push_back("imul");
	this->op_names.push_back("in");
	this->op_names.push_back("inc");
	this->op_names.push_back("in");
	this->op_names.push_back("ins");
	this->op_names.push_back("int");
	this->op_names.push_back("int3");
	this->op_names.push_back("ja");
	this->op_names.push_back("jae");
	this->op_names.push_back("jb");
	this->op_names.push_back("jbe");
	this->op_names.push_back("je");
	this->op_names.push_back("jg");
	this->op_names.push_back("jge");
	this->op_names.push_back("jg");
	this->op_names.push_back("jl");
	this->op_names.push_back("jle");
	this->op_names.push_back("jmp");
	this->op_names.push_back("jne");
	this->op_names.push_back("jno");
	this->op_names.push_back("jnp");
	this->op_names.push_back("jns");
	this->op_names.push_back("jo");
	this->op_names.push_back("jp");
	this->op_names.push_back("jrcxz");
	this->op_names.push_back("js");
	this->op_names.push_back("lahf");
	this->op_names.push_back("lea");
	this->op_names.push_back("leave");
	this->op_names.push_back("lock");
	this->op_names.push_back("lods");
	this->op_names.push_back("loop");
	this->op_names.push_back("loope");
	this->op_names.push_back("loop");
	this->op_names.push_back("loopne");
	this->op_names.push_back("lsl");
	this->op_names.push_back("maskmovq");
	this->op_names.push_back("movabs");
	this->op_names.push_back("mov");
	this->op_names.push_back("movaps");
	this->op_names.push_back("movdqa");
	this->op_names.push_back("movdqu");
	this->op_names.push_back("movs");
	this->op_names.push_back("movsxd");
	this->op_names.push_back("movsx");
	this->op_names.push_back("movups");
	this->op_names.push_back("movzx");
	this->op_names.push_back("mul");
	this->op_names.push_back("neg");
	this->op_names.push_back("nop");
	this->op_names.push_back("not");
	this->op_names.push_back("or");
	this->op_names.push_back("out");
	this->op_names.push_back("outs");
	this->op_names.push_back("paddb");
	this->op_names.push_back("pop");
	this->op_names.push_back("popf");
	this->op_names.push_back("psadbw");
	this->op_names.push_back("psubb");
	this->op_names.push_back("psubd");
	this->op_names.push_back("psubw");
	this->op_names.push_back("push");
	this->op_names.push_back("pushf");
	this->op_names.push_back("push");
	this->op_names.push_back("pxor");
	this->op_names.push_back("rcl");
	this->op_names.push_back("rcr");
	this->op_names.push_back("rep");
	this->op_names.push_back("repnz");
	this->op_names.push_back("repz");
	this->op_names.push_back("ret");
	this->op_names.push_back("retf");
	this->op_names.push_back("rex");
	this->op_names.push_back("rex.B");
	this->op_names.push_back("rex.RB");
	this->op_names.push_back("rex.R");
	this->op_names.push_back("rex.RX");
	this->op_names.push_back("rex.RXB");
	this->op_names.push_back("rex.W");
	this->op_names.push_back("rex.WB");
	this->op_names.push_back("rex.WR");
	this->op_names.push_back("rex.WRB");
	this->op_names.push_back("rex.WRX");
	this->op_names.push_back("rex.WRXB");
	this->op_names.push_back("rex.WX");
	this->op_names.push_back("rex.XB");
	this->op_names.push_back("rex.X");
	this->op_names.push_back("rol");
	this->op_names.push_back("ror");
	this->op_names.push_back("sahf");
	this->op_names.push_back("sar");
	this->op_names.push_back("sbb");
	this->op_names.push_back("scas");
	this->op_names.push_back("seta");
	this->op_names.push_back("setb");
	this->op_names.push_back("setbe");
	this->op_names.push_back("sete");
	this->op_names.push_back("setg");
	this->op_names.push_back("setge");
	this->op_names.push_back("setg");
	this->op_names.push_back("setl");
	this->op_names.push_back("setle");
	this->op_names.push_back("setne");
	this->op_names.push_back("sgdt");
	this->op_names.push_back("shl");
	this->op_names.push_back("shr");
	this->op_names.push_back("sidt");
	this->op_names.push_back("sldt");
	this->op_names.push_back("ss");
	this->op_names.push_back("stc");
	this->op_names.push_back("std");
	this->op_names.push_back("sti");
	this->op_names.push_back("stos");
	this->op_names.push_back("str");
	this->op_names.push_back("sub");
	this->op_names.push_back("syscall");
	this->op_names.push_back("xchg");
	this->op_names.push_back("xlat");
	this->op_names.push_back("xor");		
}

/*
void LexerState::__init_op_dispatch(){
	return NULL;
}*/

void LexerState::__init_memsize_names(){
	this->memsize_names.push_back("XMMWORD");
	this->memsize_names.push_back("QWORD");
	this->memsize_names.push_back("DWORD");
	this->memsize_names.push_back("WORD");
	this->memsize_names.push_back("BYTE");
}
void LexerState::__init_token_names(){
	
	this->token_names.push_back("EOF_TOKEN");
	this->token_names.push_back("OP_TOKEN");
	this->token_names.push_back("REG_TOKEN");
	this->token_names.push_back("SEGREG_TOKEN");
	this->token_names.push_back("MEMREF_TOKEN");
	this->token_names.push_back("MEMSIZE_TOKEN");
	this->token_names.push_back("NUMERIC_TOKEN");
}
void LexerState::__log_format(const char* _function,const char*_message){


	if (this->__current_char == '\n'){
		std::printf(LOG_FORMAT,\
				this->__current_token,\
				 '_',\
				_function, _message);
		
	}
	else{
		std::printf(LOG_FORMAT,\
				this->__current_token,\
				 this->__current_char,\
				_function, _message);
		
	}
}

LexerState::LexerState(std::string &source_filename){
	this->_source_filename = source_filename.c_str();
	int ret = this->__init_source();
	if (ret == -1){
		this->__log_format("LexerState::LexerState","[x] couldn't open source file");
	}
	//this->__init_token_names();
}

int LexerState::__init_source(){
	this->_source_stream = fopen(this->_source_filename.c_str(),"r");
	if (!_source_stream){
		return -1;
	}
	this->__log_format("LexerState::LexerState","[i] successfully fopen'd sourced file");
	return 0;
}

char LexerState::__next_char(){

	if (!this->_source_stream){
		this->__log_format("LexerState::__next_char()","[x] source stream is not initialized, couldn't open file?");
		return -1;
	}

	this->__current_char = fgetc(this->_source_stream);
	this->__current_pos++;
	return this->__current_char;	
}
void LexerState::__skip_spaces(){
	
	while(isspace(this->__current_char)
		 || this->__current_char == '\n'
		 || this->__current_char == '\t'
		 || this->__current_char == '\r'){
		this->next_char();
	}	

}
void LexerState::next_char(){
	this->cursor_mutex.lock();
	this->__current_char = this->__next_char();
	this->cursor_mutex.unlock();
	this->__current_pos++;
}

bool LexerState::is_memsize_name(std::string &arg){
	return std::find(this->memsize_names.begin(), 
				this->memsize_names.end(), 
					arg.c_str()) != this->memsize_names.end();
}

bool LexerState::is_segreg_name(std::string &arg){

	//this->__log_format("Parser::is_segreg_name()",this->identity_string.c_str());
	return std::find(this->segreg_names.begin(), 
				this->segreg_names.end(), 
					arg.c_str()) != this->segreg_names.end();
}
bool LexerState::is_reg_name(std::string &arg){

	//this->__log_format("Parser::is_reg_name()",this->identity_string.c_str());
	return std::find(this->reg_names.begin(), 
				this->reg_names.end(), 
					arg.c_str()) != this->reg_names.end();
}
bool LexerState::is_op_string(std::string &arg){

	/*
	for (std::string s : this->op_names){
		if (s.compare(arg) == 0){
			return true;
		}
	}
	return false; */
	return std::find(this->op_names.begin(), 
				this->op_names.end(), 
					arg.c_str()) != this->op_names.end();

}

Parser::Parser(std::string &filename){
	
	this->__current_char = ' ';
	this->__current_pos = 0;
	this->__current_token = BEGIN_TOKEN;
	this->__init_op_names();
	//this->__init_op_dispatch();
	this->__init_reg_names();
	this->__init_segreg_names();
	this->__init_memsize_names();
	this->__init_precedence();

	this->__log_format("Parser::Parser","[i] initializing source code stream");
	this->_source_filename = filename;
	if (this->__init_source() != 0){
		this->__log_format("Parser::Parser","[x] problem initializing source code");
		return;
	}
}


