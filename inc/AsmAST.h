#ifndef __AST__
#define __AST__
#include <string>
#include <vector>
/*
movAST  [regAST[ 'rdi'] memRef [  QWORD SegRegAST[ 'fs' (numAST[ '0x88'])]]]

*/
using namespace std;
class BaseAST {
	
	public:
		virtual ~BaseAST() {};
		//virtual Value* Codegen(); 
		std::string dump_str(){
			return "(base)";
		}
};


class NumericAST : public BaseAST {
	std::string value;
	public:
		NumericAST (std::string _value) : value(_value) {}
		std::string get_value(){
			return value;
		}
		std::string dump_str(){
			return "numAST[ '0x"+value+"']";
		}
		virtual ~NumericAST() {};
		
		//virtual Value* Codegen();
};

class RegAST : public BaseAST {
	std::string reg_name;
	public:
		RegAST(std::string _name) : reg_name(_name) {}
		std::string get_name(){
			return reg_name;
		}
		std::string dump_str(){
			std::string dump_str;
			if (!reg_name.empty()){
				dump_str = "regAST[ '" +reg_name + "']";
			}
			else{ 
				dump_str = "regAST[ '']";
			}
			return dump_str;
		}
		virtual ~RegAST() {};
};

/*segment registers*/
class SegRegAST : public BaseAST {
	std::string reg_name;
	NumericAST* offset; 

	public:
		SegRegAST(std::string _name, NumericAST* _offset) : reg_name(_name), offset(_offset)  {}
		virtual ~SegRegAST() {};

		std::string dump_str(){
			std::string dump_str;
			dump_str = "SegRegAST[ '']";
			if (!reg_name.empty() && offset != nullptr){
				dump_str = "SegRegAST[ '" +reg_name+ "' ("+offset->dump_str()+")]";
			}
			return dump_str;
		}
};

/**
class BinOpAST : public Op2AST {
	std::string bin_operator;
	
	BaseAST *LHS, *RHS;
	public:
		BinOpAST(std::string op, BaseAST* lhs, BaseAST* rhs) :
			bin_operator(op), Op2AST(lhs,rhs) {}

		virtual ~BinOpAST() {};
		std::string dump_str(){
			std::string _dump_string = "binOpAST[{" + this->bin_operator +"}  ";
			_dump_string += Op2AST::dump_str(" ");
			_dump_string += "]";
			return _dump_string;
		}
}; **/

class BinOpAST : public BaseAST {
	std::string bin_operator;
	
	BaseAST *LHS, *RHS;
	public:
		BinOpAST(std::string op, BaseAST* lhs, BaseAST* rhs) :
			bin_operator(op), LHS(lhs), RHS(rhs) {}

		virtual ~BinOpAST() {};
		std::string dump_str(){
			std::string _dump_string = "binOpAST[{" + this->bin_operator +"}  ";
			if (dynamic_cast<RegAST*>(LHS)){
				_dump_string += ((RegAST* ) LHS)->dump_str() + "  ";
			}else if (dynamic_cast<NumericAST*>(LHS)){
				_dump_string += ((NumericAST* ) LHS)->dump_str() + "  ";
			}else if (dynamic_cast<BinOpAST*>(LHS)){
				
				_dump_string += ((BinOpAST* ) LHS)->dump_str() + "  ";
			}

			if (dynamic_cast<RegAST*>(RHS)){
				_dump_string += ((RegAST* ) RHS)->dump_str();
			}else if (dynamic_cast<NumericAST*>(RHS)){
				_dump_string += ((NumericAST* ) RHS)->dump_str();
			}else if (dynamic_cast<BinOpAST*>(RHS)){
				
				_dump_string += ((BinOpAST* ) RHS)->dump_str();
			}

			_dump_string += "]";
			return _dump_string;
		}
}; 

class MemRefAST : public BaseAST { 
	/*might not use all of these
		[base (+) ( index (*) scale ) (+) disp]
		[rax + rbx * 1 + 0x8]
	*/
	std::string size_type;
	BaseAST* base;  /*register, important this is a register */
	RegAST* reg_base;

	public:
		MemRefAST(BaseAST* _base, std::string _size_type): 
			base(_base), size_type(_size_type){};  /*more commonly used form*/

		MemRefAST(BaseAST* _base): 
			base(_base){};  /*more commonly used form*/

		/*	
		MemRefAST(RegAST* _base, std::string _size_type): 
			reg_base(_base), size_type(_size_type){};*/

		MemRefAST(){};  /*more commonly used form*/

		BaseAST *get_base(){
			return base;
		}
		std::string get_size_type(){
			return size_type;
		}
		std::string dump_str(){
			std::string dump_str = "memRef[  ";
			if (!size_type.empty()){
				dump_str += size_type +" ";
			}
			if (base == nullptr){
				dump_str += " ()]";
				return dump_str;
			}
			
			if (dynamic_cast<SegRegAST*>(base) != nullptr){
				dump_str += ((SegRegAST* )base)->dump_str();
			}
			else if (dynamic_cast<RegAST*>(base) != nullptr){
				dump_str += ((RegAST* )base)->dump_str();
			}
			else if (dynamic_cast<BinOpAST*>(base) != nullptr){
				dump_str += ((BinOpAST* ) base)->dump_str();
				//dump_str += "binOpAST[ ()]"; //((BinOpAST* ) base)->dump_str();
			}
			dump_str += "]";
			return dump_str;
		}
		
		
};

class Op1AST : public BaseAST { 
	BaseAST* arg;

	public: 
		Op1AST(BaseAST* _arg) : arg(_arg) {};
		Op1AST(){};
		virtual ~Op1AST(){}; 

		std::string dump_str(std::string op_name){
			std::string _dump_str;

			_dump_str += op_name + "[ ";
			if (arg == nullptr){
				_dump_str += "]";
				return _dump_str;
			}
		
			if (dynamic_cast<RegAST*>(arg) != nullptr){
				_dump_str += ((RegAST*) arg)->dump_str();
			}
			else if (dynamic_cast<MemRefAST*>(arg) != nullptr){
				_dump_str += ((MemRefAST*) arg)->dump_str();
			}
			else if (dynamic_cast<SegRegAST*>(arg) != nullptr){
				_dump_str += ((SegRegAST*) arg)->dump_str();
			}
			else if (dynamic_cast<NumericAST*>(arg) != nullptr){
				_dump_str += ((NumericAST*) arg)->dump_str();
			}

			_dump_str += "]";
			return _dump_str;					
		}
};


class Op2AST : public BaseAST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		Op2AST(BaseAST* _src, BaseAST* _dst) : src(_src), dst(_dst) {};
		Op2AST(){};
		virtual ~Op2AST(){}; 
		std::string dump_str(std::string op_name){
			std::string _dump_str;
			//add if statement for numeric src as well	
			_dump_str += op_name + "[ ";
			if (dst == nullptr || src == nullptr){
				_dump_str += "]";
				return _dump_str;
			}
		
			/** src **/
			//the dynamic cast is crashing becuase its being given a base type!
			if (dynamic_cast<MemRefAST*>(dst) != nullptr){
				_dump_str += ((MemRefAST*) dst)->dump_str() +"  ";
			}
			else if (dynamic_cast<RegAST*>(dst) != nullptr){
				_dump_str += ((RegAST*) dst)->dump_str() +"  ";
			}
			
			else if (dynamic_cast<SegRegAST*>(dst) != nullptr){
				_dump_str += ((SegRegAST*) dst)->dump_str() +" ";
			}
			else if (dynamic_cast<NumericAST*>(dst) != nullptr){
				_dump_str += ((NumericAST*) dst)->dump_str() +"  ";
			}


			/** dst **/

			if (dynamic_cast<MemRefAST*>(src) != nullptr){
				_dump_str += ((MemRefAST*) src)->dump_str();
				//_dump_str += "memRefAst [ ...]";
			}
			else if (dynamic_cast<RegAST*>(src) != nullptr){
				_dump_str += ((RegAST*) src)->dump_str();
				//_dump_str += "regAst [ ...]";
			}
			else if (dynamic_cast<SegRegAST*>(src) != nullptr){
				_dump_str += ((SegRegAST*) src)->dump_str();
			}
			else if (dynamic_cast<NumericAST*>(src) != nullptr){
				_dump_str += ((NumericAST*) src)->dump_str();
			}
			_dump_str += "]";
			return _dump_str;					
		}
};


class AdcAST : public Op2AST { 
	/*Add with carry*/
		
	public: 
		AdcAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){} 
		AdcAST(){} 
		virtual ~AdcAST() {}
		std::string dump_str(){
			return Op2AST::dump_str("adcAST");
		}
};
class Addr32AST : public BaseAST { public: Addr32AST(){} };
class AddAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		AddAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		AddAST(){};
		virtual ~AddAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("addAST");
		}
}; 

class AndAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		AndAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		AndAST(){};
		virtual ~AndAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("andAST");
		}
}; 


class CallAST : public Op1AST { 
	RegAST* rip;	
	public: 
		CallAST(BaseAST* _target) : Op1AST(_target){
			rip = new RegAST("rip");
		};

		CallAST(){};
		virtual ~CallAST(){}; 
		std::string dump_str(){
			return Op1AST::dump_str("callAST");
		}
};

class CdqAST : public BaseAST { 
	RegAST* dst_1; //dst
	RegAST* dst_2; //dst
	RegAST* src; //src
	
	public: 
		CdqAST(){
			dst_1 = new RegAST("edx");
			dst_2 = new RegAST("eax");
			src = new RegAST("eax");
		} 
		std::string dump_str(){
			std::string _dump_str;
			_dump_str = "cdqAST[ ";
			_dump_str += ((RegAST*) dst_1)->dump_str() + " ";
			_dump_str += ((RegAST*) dst_2)->dump_str() + " ";
			_dump_str += ((RegAST*) src)->dump_str() + "]";
			return _dump_str;	
		}	
};
class CdqeAST : public BaseAST { 
	RegAST* dst; //dst
	RegAST* src; //src
	
	public: 
		CdqeAST(){
			dst = new RegAST("rax");
			src = new RegAST("eax");
		} 
		std::string dump_str(){
			std::string _dump_str;
			_dump_str = "cdqeAST[ ";
			_dump_str += ((RegAST*) dst)->dump_str() + " ";
			_dump_str += ((RegAST*) src)->dump_str() + "]";
			return _dump_str;	
		}	
};

class CwdAST : public BaseAST { 
	RegAST* dst_1; //dst
	RegAST* dst_2; //dst
	RegAST* src; //src
	
	public: 
		CwdAST(){

			dst_1 = new RegAST("edx");
			dst_2 = new RegAST("eax");
			src = new RegAST("ax");
		} 
		std::string dump_str(){
			std::string _dump_str;
			_dump_str = "cwdAST[ ";
			_dump_str += ((RegAST*) dst_1)->dump_str() + " ";
			_dump_str += ((RegAST*) dst_2)->dump_str() + " ";
			_dump_str += ((RegAST*) src)->dump_str() + "]";
			return _dump_str;	
		}	
};

class CwdeAST : public BaseAST { 
	RegAST* dst_1; //dst
	RegAST* dst_2; //dst
	RegAST* src; //src
	
	public: 
		CwdeAST(){

			dst_1 = new RegAST("edx");
			dst_2 = new RegAST("eax");
			src = new RegAST("ax");
		} 
		std::string dump_str(){
			std::string _dump_str;
			_dump_str = "cwdeAST[ ";
			_dump_str += ((RegAST*) dst_1)->dump_str() + " ";
			_dump_str += ((RegAST*) dst_2)->dump_str() + " ";
			_dump_str += ((RegAST*) src)->dump_str() + "]";
			return _dump_str;	
		}	
};

class CbwAST : public BaseAST { 
	RegAST* dst; //dst
	RegAST* src; //src
	
	public: 
		CbwAST(){
			src = new RegAST("ax");
			dst = new RegAST("al");
		} 
		std::string dump_str(){
			std::string _dump_str;
			_dump_str = "cbwAST[ ";
			_dump_str += ((RegAST*) dst)->dump_str() + " ";
			_dump_str += ((RegAST*) src)->dump_str() + "]";

			return _dump_str;	
		}	
};


class CqoAST : public BaseAST { 
	RegAST* dst; //dst
	RegAST* src; //src
	
	public: 
		CqoAST(){
			src = new RegAST("rdx");
			dst = new RegAST("rax");
		} 
		std::string dump_str(){
			std::string _dump_str;
			_dump_str = "cqoAST[ ";
			_dump_str += ((RegAST*) dst)->dump_str() + " ";
			_dump_str += ((RegAST*) src)->dump_str() + "]";

			return _dump_str;	
		}	
};


class ClcAST : public BaseAST { public: ClcAST(){} };
class CldAST : public BaseAST { public: CldAST(){} };
class CliAST : public BaseAST { public: CliAST(){} };
class CmcAST : public BaseAST { public: CmcAST(){} };

class CmovaeAST : public BaseAST { public: CmovaeAST(){} };
class CmovaAST : public BaseAST { public: CmovaAST(){} };
class CmovbeAST : public BaseAST { public: CmovbeAST(){} };
class CmovbAST : public BaseAST { public: CmovbAST(){} };
class CmoveAST : public BaseAST { public: CmoveAST(){} };
class CmovgeAST : public BaseAST { public: CmovgeAST(){} };
class CmovgAST : public BaseAST { public: CmovgAST(){} };
class CmovleAST : public BaseAST { public: CmovleAST(){} };
class CmovlAST : public BaseAST { public: CmovlAST(){} };
class CmovneAST : public BaseAST { public: CmovneAST(){} };
class CmovnsAST : public BaseAST { public: CmovnsAST(){} };
class CmovsAST : public BaseAST { public: CmovsAST(){} };

class CmpAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;
	
	public: 
		CmpAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		CmpAST(){};
		virtual ~CmpAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("cmpAST[ ");
		}
}; 

class CmpsAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		CmpsAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		CmpsAST(){};
		virtual ~CmpsAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("cmpsAST[ ");
		}
}; 


class CmpswAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		CmpswAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		CmpswAST(){};
		virtual ~CmpswAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("cmpswAST[ ");
		}
}; 

class CmpsbAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		CmpsbAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		CmpsbAST(){};
		virtual ~CmpsbAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("cmpsbAST[ ");
		}
}; 

class CmpsdAST : public Op2AST { 
	/*
	BaseAST* src;
	BaseAST* dst;*/

	public: 
		CmpsdAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		CmpsdAST(){};
		virtual ~CmpsdAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("cmpsdAST[ ");
		}
}; 


class CmpsqAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		CmpsqAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		CmpsqAST(){};
		virtual ~CmpsqAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("cmpsqAST[ ");
		}
}; 

class DaaAST : public BaseAST { public: DaaAST(){} };

class DecAST : public Op1AST { 
	BaseAST* dst;
	public: 
		DecAST(BaseAST* _dst) : Op1AST(dst) {};
		virtual ~DecAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("decAST[ ");
		}
}; 

class DivAST : public Op1AST { 
		
	public: 
		DivAST(BaseAST* _dst) : Op1AST(_dst){};		
		virtual ~DivAST() {};

		std::string dump_str(){
			return Op1AST::dump_str("divAST[ ");
		}

};

class DsAST : public BaseAST { public: DsAST(){} };
class EsAST : public BaseAST { public: EsAST(){} };
class FcomipAST : public BaseAST { public: FcomipAST(){} };
class FcompAST : public BaseAST { public: FcompAST(){} };
class FcomAST : public BaseAST { public: FcomAST(){} };
class FdivpAST : public BaseAST { public: FdivpAST(){} };
class FdivAST : public BaseAST { public: FdivAST(){} };
class FdivrAST : public BaseAST { public: FdivrAST(){} };
class FdivrpAST : public BaseAST { public: FdivrpAST(){} };
class FiaddAST : public BaseAST { public: FiaddAST(){} };
class FicompAST : public BaseAST { public: FicompAST(){} };
class FidivrAST : public BaseAST { public: FidivrAST(){} };
class FidivAST : public BaseAST { public: FidivAST(){} };
class FildAST : public BaseAST { public: FildAST(){} };
class FimulAST : public BaseAST { public: FimulAST(){} };
class FisttpAST : public BaseAST { public: FisttpAST(){} };
class FldAST : public BaseAST { public: FldAST(){} };
class FldzAST : public BaseAST { public: FldzAST(){} };
class FmulAST : public BaseAST { public: FmulAST(){} };
class FpremAST : public BaseAST { public: FpremAST(){} };
class Fprem1AST : public BaseAST { public: Fprem1AST(){} };
class FscaleAST : public BaseAST { public: FscaleAST(){} };
class FsqrtAST : public BaseAST { public: FsqrtAST(){} };
class FsAST : public BaseAST { public: FsAST(){} };
class FstpAST : public BaseAST { public: FstpAST(){} };
class FsubAST : public BaseAST { public: FsubAST(){} };
class FsubpAST : public BaseAST { public: FsubpAST(){} };
class FsubrAST : public BaseAST { public: FsubrAST(){} };
class FucomppAST : public BaseAST { public: FucomppAST(){} };
class FwaitAST : public BaseAST { public: FwaitAST(){} };
class IcebpAST : public BaseAST { public: IcebpAST(){} };
class IdivAST : public BaseAST { public: IdivAST(){} };
class ImulAST : public BaseAST { public: ImulAST(){} };
class IncAST : public BaseAST { public: IncAST(){} }; //single operand
class InAST : public BaseAST { public: InAST(){} }; //single operand
class InsAST : public BaseAST { public: InsAST(){} };
class IntAST : public BaseAST { public: IntAST(){} };
class Int3AST : public BaseAST { public: Int3AST(){} };

class JaAST : public Op1AST { 
	public: 
		JaAST(BaseAST* _dst) : Op1AST(_dst){};
		JaAST() {};
		virtual ~JaAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jaAST");
		}
};
class JaeAST : public Op1AST {
	public: 
		JaeAST(BaseAST* _dst) : Op1AST(_dst){};
		JaeAST() {};
		virtual ~JaeAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jaeAST");
		}
};

class JbAST : public Op1AST {
	public: 
		JbAST(BaseAST* _dst) : Op1AST(_dst){};
		JbAST() {};
		virtual ~JbAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jbAST");
		}
};



class JbeAST : public Op1AST {
	public: 
		JbeAST(BaseAST* _dst) : Op1AST(_dst){};
		JbeAST() {};
		virtual ~JbeAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jbeAST");
		}
};



class JeAST : public Op1AST {
	public: 
		JeAST(BaseAST* _dst) : Op1AST(_dst){};
		JeAST() {};
		virtual ~JeAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jeAST");
		}
};


class JgeAST : public Op1AST {
	public: 
		JgeAST(BaseAST* _dst) : Op1AST(_dst){};
		JgeAST() {};
		virtual ~JgeAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jgeAST");
		}
};

class JgAST : public Op1AST {
	public: 
		JgAST(BaseAST* _dst) : Op1AST(_dst){};
		JgAST() {};
		virtual ~JgAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jgAST");
		}
};

class JlAST : public Op1AST {
	public: 
		JlAST(BaseAST* _dst) : Op1AST(_dst){};
		JlAST() {};
		virtual ~JlAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jlAST");
		}
};


class JleAST : public Op1AST {
	public: 
		JleAST(BaseAST* _dst) : Op1AST(_dst){};
		JleAST() {};
		virtual ~JleAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jleAST");
		}
};

class JmpAST : public Op1AST { //unconditional jmp 
	public: 
		JmpAST(BaseAST* _dst) : Op1AST(_dst){};
		JmpAST() {};
		virtual ~JmpAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jmpAST");
		}
};

class JneAST : public Op1AST { 
	public: 
		JneAST(BaseAST* _dst) : Op1AST(_dst){};
		JneAST() {};
		virtual ~JneAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jneAST");
		}
};

class JnoAST : public Op1AST { 
	public: 
		JnoAST(BaseAST* _dst) : Op1AST(_dst){};
		JnoAST() {};
		virtual ~JnoAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jnoAST");
		}
};


class JnpAST : public Op1AST { 
	public: 
		JnpAST(BaseAST* _dst) : Op1AST(_dst){};
		JnpAST() {};
		virtual ~JnpAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jnpAST");
		}
};

class JnsAST : public Op1AST { 
	public: 
		JnsAST(BaseAST* _dst) : Op1AST(_dst){};
		JnsAST() {};
		virtual ~JnsAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jnsAST");
		}
};


class JoAST : public Op1AST { 
	public: 
		JoAST(BaseAST* _dst) : Op1AST(_dst){};
		JoAST() {};
		virtual ~JoAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("joAST");
		}
};


class JpAST : public Op1AST { 
	public: 
		JpAST(BaseAST* _dst) : Op1AST(_dst){};
		JpAST() {};
		virtual ~JpAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jpAST");
		}
};

class JrcxzAST : public BaseAST { public: JrcxzAST(){} };

class JsAST : public Op1AST { 
	public: 
		JsAST(BaseAST* _dst) : Op1AST(_dst){};
		JsAST() {};
		virtual ~JsAST(){};
		std::string dump_str(){
			return Op1AST::dump_str("jsAST");
		}
};

class LahfAST : public BaseAST { public: LahfAST(){} };

class LeaAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		LeaAST(BaseAST* src, BaseAST* dst) : Op2AST(src,dst) {}
		virtual ~LeaAST(){} 

		std::string dump_str(){
			return Op2AST::dump_str("leaAST");
		}
};

class LeaveAST : public BaseAST { public: LeaveAST(){} };
class LockAST : public BaseAST { public: LockAST(){} };
class LodsAST : public BaseAST { public: LodsAST(){} };
class LoopeAST : public BaseAST { public: LoopeAST(){} };
class LoopAST : public BaseAST { public: LoopAST(){} };
class LoopneAST : public BaseAST { public: LoopneAST(){} };
class LslAST : public BaseAST { public: LslAST(){} };
class MaskmovqAST : public BaseAST { public: MaskmovqAST(){} };
class MovabsAST : public BaseAST { public: MovabsAST(){} };


class MovAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		MovAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		BaseAST* get_src(){
			return src;
		}
		void set_src(BaseAST* _src){
			src = _src;
		}
		MovAST(){};
		virtual ~MovAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("movAST");
		}
}; 

/*some of these have implicit operands*/
class MovapsAST : public BaseAST { public: MovapsAST(){} };
class MovdqaAST : public BaseAST { public: MovdqaAST(){} };
class MovdquAST : public BaseAST { public: MovdquAST(){} };
class MovsAST : public BaseAST { public: MovsAST(){} };
class MovsxdAST : public BaseAST { public: MovsxdAST(){} };
class MovsxAST : public BaseAST { public: MovsxAST(){} };
class MovupsAST : public BaseAST { public: MovupsAST(){} };
class MovzxAST : public BaseAST { public: MovzxAST(){} };

class MulAST : public Op1AST { 
	public: 
		MulAST(BaseAST* _arg) : Op1AST(_arg){};
		virtual ~MulAST() {};
		std::string dump_str(){
			return Op1AST::dump_str("mulAST");
		}		
		
};
class NegAST : public Op1AST { 
	public: 
		NegAST(BaseAST* _arg) : Op1AST(_arg){};
		virtual ~NegAST() {};
		std::string dump_str(){
			return Op1AST::dump_str("negAST");
		}		
		
};

class NopAST : public Op2AST { 
	
	public: 
		NopAST(BaseAST* src, BaseAST* dst) : Op2AST(src,dst){};
		virtual ~NopAST(){};
		std::string dump_str(){
			return Op2AST::dump_str("nopAST");
		}
};
class NotAST : public Op1AST { 
	public: 
		NotAST(BaseAST *_arg) : Op1AST(_arg){} 
		virtual ~NotAST(){}
		std::string dump_str(){
			return Op1AST::dump_str("notAST");
		}
};

class OrAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		OrAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		OrAST(){};
		virtual ~OrAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("orAST");
		}
}; 

class OutAST : public BaseAST { public: OutAST(){} }; //implicit operands
class OutsAST : public BaseAST { public: OutsAST(){} }; //implicit operands

class PaddbAST : public BaseAST { public: PaddbAST(){} };

class PopfAST : public BaseAST { public: PopfAST(){} };
class PopAST : public BaseAST { public: PopAST(){} };

class PsadbwAST : public BaseAST { public: PsadbwAST(){} };
class PsubbAST : public BaseAST { public: PsubbAST(){} };
class PsubdAST : public BaseAST { public: PsubdAST(){} };
class PsubwAST : public BaseAST { public: PsubwAST(){} };
class PushfAST : public BaseAST { public: PushfAST(){} };

class PushAST : public BaseAST { 
	BaseAST* reg;
	public: 
		PushAST(BaseAST* _reg) : reg(_reg){};
		PushAST(){};

		virtual ~PushAST(){};
	
		std::string dump_str(){
			std::string dump_str;
			dump_str += "pushAST [ " + ((RegAST*) reg)->dump_str() +"]";
			return dump_str;				
		}
};

class PxorAST : public BaseAST { public: PxorAST(){} };
class RAST : public BaseAST { public: RAST(){} };

class RclAST :  public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		RclAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		RclAST(){};
		virtual ~RclAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("rclAST");
		}
}; 

class RcrAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		RcrAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		RcrAST(){};
		virtual ~RcrAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("rcrAST");
		}
}; 

class RepnzAST : public BaseAST { public: RepnzAST(){} };
class RepAST : public BaseAST { public: RepAST(){} };
class RepzAST : public BaseAST { public: RepzAST(){} };
class RetAST : public BaseAST { public: RetAST(){} };
class RetfAST : public BaseAST { public: RetfAST(){} };
class RexAST : public BaseAST { public: RexAST(){} };

class RolAST : public Op2AST { 
	public: 
		RolAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){} 
		virtual ~RolAST(){};
		std::string dump_str(){
			return Op2AST::dump_str("rolAST");
		}
};
class RorAST : public Op2AST { 
	public: 
		RorAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){} 
		virtual ~RorAST(){};
		std::string dump_str(){
			return Op2AST::dump_str("rorAST");
		}
};

class SahfAST : public BaseAST { public: SahfAST(){} };
class SarAST : public BaseAST { public: SarAST(){} };

class SbbAST : public BaseAST { public: SbbAST(){} };
class ScasAST : public BaseAST { public: ScasAST(){} };
class SetaAST : public BaseAST { public: SetaAST(){} };
class SetbeAST : public BaseAST { public: SetbeAST(){} };
class SetbAST : public BaseAST { public: SetbAST(){} };
class SeteAST : public BaseAST { public: SeteAST(){} };
class SetgeAST : public BaseAST { public: SetgeAST(){} };
class SetgAST : public BaseAST { public: SetgAST(){} };
class SetleAST : public BaseAST { public: SetleAST(){} };
class SetlAST : public BaseAST { public: SetlAST(){} };
class SetneAST : public BaseAST { public: SetneAST(){} };
class SgdtAST : public BaseAST { public: SgdtAST(){} };

class ShlAST : public Op2AST { 
	public: 
		ShlAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		virtual ~ShlAST(){};
		std::string dump_str(){
			return Op2AST::dump_str("shlAST[ ");
		}
};

class ShrAST : public Op2AST { 
	public: 
		ShrAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		ShrAST(){};
		virtual ~ShrAST(){};
		std::string dump_str(){
			return Op2AST::dump_str("shrAST");
		}
};

class SidtAST : public BaseAST { public: SidtAST(){} };
class SldtAST : public BaseAST { public: SldtAST(){} };
class SsAST : public BaseAST { public: SsAST(){} };
class StcAST : public BaseAST { public: StcAST(){} };
class StdAST : public BaseAST { public: StdAST(){} };
class StiAST : public BaseAST { public: StiAST(){} };
class StosAST : public BaseAST { public: StosAST(){} };
class StrAST : public BaseAST { public: StrAST(){} };

class SubAST : public Op2AST { 
	public: 
		SubAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		SubAST(){} 
		std::string dump_str(){
			return Op2AST::dump_str("subAST");
		}

		
};

class SyscallAST : public BaseAST { public: SyscallAST(){} };
class TestAST : public Op2AST {
	public:
		TestAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		TestAST() {};
		virtual ~TestAST(){};
		std::string dump_str(){
			return Op2AST::dump_str("testAST");
		}
		
};

class XchgAST : public Op2AST {
	public: 
		XchgAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};
		XchgAST(){} 
		std::string dump_str(){
			return Op2AST::dump_str("xchgAST");
		}
};
class XlatAST : public BaseAST { public: XlatAST(){} };

class XorAST : public Op2AST { 
	BaseAST* src;
	BaseAST* dst;

	public: 
		XorAST(BaseAST* _src, BaseAST* _dst) : Op2AST(_src,_dst){};

		XorAST(){};
		virtual ~XorAST(){}; 
		std::string dump_str(){
			return Op2AST::dump_str("xorAST");
		}

};

#endif
