#include "AsmAST.h"
#include "AsmParser.h"

#include <vector>
#include <math.h>
#include <map>
#include <iostream>
#include <mutex>

using namespace std;

BaseAST* Parser::base_parser(){

		switch(this->lex_token()){
			case OP_TOKEN: 
				return this->__dispatch_op();break;
			case MEMSIZE_TOKEN:
				this->__log_format("Parser::base_parser()","[i] pasing memsize..."); 
				return this->__dispatch_memref();break;
			default: 
				//this->__log_format("Parser::base_parser()","[i] defaulted"); 
				break;
		}
		return NULL;
}

char LexerState::lex_token(){
	//assignee of the holy this->__current_token		
	std::string log_string;
	//this->__log_format("LexerState::lex_token()","[i] lexing token...");
	this->__skip_spaces();
	if (this->__current_char == '#'){
		while (this->__current_char != '\n'){
			this->next_char();	
		}
	}
	if (isalpha(this->__current_char)){
		
		this->identity_string = "";
		/*we can have an iden as a reg name, size name or op string*/
		while(isalnum(this->__current_char)){
			this->identity_string += this->__current_char;
			this->next_char();
		}	

		if (this->is_op_string(this->identity_string)){
			this->__current_token = OP_TOKEN;
			log_string.append("[i] TOKEN => OP [");
			log_string.append(this->identity_string);
			log_string.append("]");
			this->__log_format("LexerState::lex_token()",log_string.c_str());

			return OP_TOKEN;
		}

		if (this->is_memsize_name(this->identity_string)){
			this->__current_token = MEMSIZE_TOKEN;
			log_string = "[i] TOKEN => MEMSIZE [";
			log_string += this->identity_string;
			log_string += "]";

			this->__log_format("LexerState::lex_token()",log_string.c_str());

			return MEMSIZE_TOKEN;
		}

		if (this->is_reg_name(this->identity_string)){
			this->__current_token = REG_TOKEN;
			log_string.append("[i] TOKEN => REG [");
			log_string.append(this->identity_string);
			log_string.append("]");
			this->__log_format("LexerState::lex_token()",log_string.c_str());

			return REG_TOKEN;
		}
		if (this->is_segreg_name(this->identity_string)){
				
			this->__current_token = SEGREG_TOKEN;
			log_string.append("[i] TOKEN => SEGREG [");
			log_string.append(this->identity_string);
			log_string.append("]");
			this->__log_format("LexerState::lex_token()",log_string.c_str());

			return SEGREG_TOKEN;
		}
	}

	if (this->__current_char == '#'){
		while(this->__current_char != '\n'){
			this->next_char();	
		}	
	}

	if (this->__current_char == '['){
		this->__current_token = MEMREF_TOKEN;
		this->__log_format("LexerState::lex_token()","[i] TOKEN => MEMREF");
		return MEMREF_TOKEN;
	}

	/*hex number*/
	if(isdigit(this->__current_char)){
		if (this->__current_char == '0'){
			this->next_char(); //skip 0
			this->next_char(); //skip x
		}
		this->numeric_string = "";
		while(isalnum(this->__current_char)) {
			this->numeric_string += this->__current_char;
			this->next_char();
		}
	
		this->_lex_numeric();
		this->__skip_spaces();	

		log_string = "[i] TOKEN => NUMERIC '"+this->numeric_string+"'";
		this->__log_format("LexerState::lex_token()",log_string.c_str());

		this->__current_token = NUMERIC_TOKEN;
		return NUMERIC_TOKEN;
	}

	if (this->__current_char == EOF) { return EOF_TOKEN; }

	//this->__log_format("LexerState::lex_token()","[i] hit bottom, skipping char");
	this->next_char();
	this->__skip_spaces();
	return this->__current_char;
}



BaseAST* Parser::__dispatch_op(){
	//find out which op needs to be parsed, dispatch to that parser
	return this->get_op(this->identity_string);
}

void LexerState::_lex_numeric(){
	this->numeric_value = std::strtod(this->numeric_string.c_str(),0);
}

vector<BaseAST*> Parser::parse(){
	vector<BaseAST*> ast;
	while(this->__current_pos >= 0){	
		BaseAST* B = this->base_parser();
		/*
		if (B == NULL){
			this->__current_pos = -1;
			break;
		}
		ast.push_back(B);
		*/
	}

	return ast;
}

std::string Parser::dump_str(BaseAST* ast){
	if (ast == nullptr){
		return "";
	}
	if (dynamic_cast<RegAST*>(ast) != nullptr){
		return ((RegAST*) ast)->dump_str();	
	}
	else if (dynamic_cast<SegRegAST*>(ast) != nullptr){
		return ((SegRegAST*) ast)->dump_str();	
	}
	else if (dynamic_cast<NumericAST*>(ast) != nullptr){
		return ((NumericAST*) ast)->dump_str();	
	}
	return "";
}
BinOpAST* Parser::binop_parser(unsigned int prec, BaseAST* _lhs){
	/*
		mov    rdi,QWORD PTR [rsi+rax*rdx]

		movAST[
			 regAST[ 'rdi'] 
			 memRef[  QWORD 
				binOpAST[{*}  
					binOpAST[{+}  
						regAST[ 'rsi']  
						regAST[ 'rax']
					]  
					regAST[ 'rdx']
				]
			]
		]



		mov    rdi,QWORD PTR [rsi*rax+rdx]
		movAST[ 
			regAST[ 'rdi']  
			memRef[  QWORD 
				binOpAST[{+}  
					binOpAST[{*}  
						regAST[ 'rsi']  
						regAST[ 'rax']
					]  
					regAST[ 'rdx']
				]
			]
		]


	*/
	std::string _nbinop;
	std::string _binop;
	std::string _cur_iden;
	std::string log_string;
	BaseAST* _cur_op;
	BinOpAST* binop_ast;
	BaseAST* _rhs;
	

	//probably add a type resolving function to dump stuff like the Alang parser

	while(this->__current_char != ']'){ //we are parsing a memref so the end is a ]
		//log_string = "[i] parsing binop with lhs -> ";

		//log_string += this->dump_str(_lhs);	
		//this->__log_format("Parser::binop_parser",log_string.c_str());

		_binop = "";
		_nbinop = "";
		//this check will be redudent
		/**
		if (!this->is_binop(this->__current_char)){ //TODO 
			return (BinOpAST*) _lhs; //we didn't 
		}**/

		_binop += this->__current_char;	 //we know this is a binop before we hit this function
		//log_string = "parsing binop ['";
		//log_string += _binop;
		//log_string += "']";	
		//this->__log_format("Parser::binop_parser()",log_string.c_str());

		this->next_char();//skip passed binop char
		char token = this->lex_token();
		_nbinop += this->__current_char;
	
		
		if (this->__current_token != REG_TOKEN && 
			this->__current_token != NUMERIC_TOKEN &&
				this->__current_token != SEGREG_TOKEN &&
					this->__current_char != ']'){

			return NULL;
		}

		if (this->__current_token == REG_TOKEN){
			_rhs = (RegAST*) this->reg_parser();		
		}

		else if (this->__current_token == NUMERIC_TOKEN){
			_rhs = (NumericAST*)this->numeric_parser(false);
		}

		if (this->is_binop(_nbinop[0])){
			
			unsigned int _binop_prec = this->get_op_precedence(_binop[0]);
			unsigned int _nbinop_prec = this->get_op_precedence(_nbinop[0]);
			
			//log_string = "next binop check : '" + _binop + "'("+to_string(_binop_prec)+")";
			//log_string += " vs '"+_nbinop+"'("+to_string(_nbinop_prec)+")";
			//this->__log_format("Parser::binop_parser()",log_string.c_str());
			/*
				mov    rdi,QWORD PTR [r13+rax*8-0x8]
				memRef[  QWORD 
					binOpAST[{-}  
						binOpAST[{*}  
							binOpAST[{+}  
								regAST[ 'r13']  
								regAST[ 'rax']
							]  
							numAST[ '0x8']
						]  
					numAST[ '0x8']
					]
				]
	
			*/
			if (_nbinop_prec > _binop_prec){
				_rhs = (BinOpAST* )this->binop_parser(_nbinop_prec-1,_rhs);
				_lhs = (BinOpAST* )new BinOpAST(_binop,_lhs,_rhs);
				//_lhs = (BinOpAST* )this->binop_parser(_nbinop_prec-1,_lhs);

			}else{
				_lhs = (BinOpAST*) new BinOpAST(_binop,_lhs,_rhs);
			}
		}
		else{	
			_lhs = new BinOpAST(_binop, _lhs, _rhs);
			
		}
	}
	//finish implementing the build_ast_string	
	//log_string = "[i] final binop -> ";
	//log_string += ((BinOpAST*)_lhs)->dump_str();
	//this->__log_format("Parser::binop_parser",log_string.c_str());
	//this->__log_format("Parser::binop_parser",((BinOpAST*)_lhs)->dump_str().c_str());
	return (BinOpAST*) _lhs;
}

bool LexerState::is_binop(char _op){
        return _op == '=' || _op == '+' || _op == '-' || _op == '/' || _op == '*';
}

MemRefAST* Parser::__dispatch_memref(){
	//iden string pointing to size
	while (this->__current_char != '['){
		this->lex_token(); //lex past the 'PTR' string since its useless
	}

	return this->memref_parser(this->identity_string);	
}
MemRefAST* Parser::memref_parser(std::string size_type){
	//gooble up first *AST and then if we have a scalar operation, transfer to the binop parser	
	//current_char pointing at '['
	MemRefAST *_memref; 
	BaseAST* base;
	BaseAST* lhs;
	
	this->__log_format("Parser::memref_parser","[i] parsing memref");

	while(this->__current_token != SEGREG_TOKEN && this->__current_token != MEMREF_TOKEN){
		this->lex_token();
	}

	if (this->__current_token != SEGREG_TOKEN){
		this->next_char();
		this->lex_token();
	}
	if (this->is_binop(this->__current_char)) {
		//should refactor this	
		switch(this->__current_token){
			case REG_TOKEN:
				//this->__log_format("Parser::memref_parser","[i] parsing register");
				lhs = this->reg_parser();break;
			case NUMERIC_TOKEN: lhs = this->numeric_parser(false);break;
		}
		_memref = new MemRefAST(this->binop_parser(0,lhs), size_type);
	} 
	else{
		//this->next_char();
		//this->lex_token();
		//this->__log_format("Parser::memref_parser","[i] non-binop memref...");
		switch(this->__current_token){
			case REG_TOKEN: 
				_memref = new MemRefAST((RegAST*) this->reg_parser(), size_type);
				break;
			case NUMERIC_TOKEN: 
				_memref = new MemRefAST((NumericAST* ) this->numeric_parser(false),size_type);
				break;
			case SEGREG_TOKEN:
				_memref = new MemRefAST((SegRegAST* ) this->segreg_parser(),size_type);
		}

	}
	this->__log_format("Parser::memref_parser",_memref->dump_str().c_str());
	return _memref;
}

void Parser::__base_parser_test(){
	//this->__log_format("Parser::__base_parser_test","[i] parsing stream...");
	this->parse();
}
void Parser::__tokenizer_test(){
	this->__log_format("Parser::__tokenizer_test","[i] tokenizing stream...");
	token_type _t;
	while ( (_t = (token_type) this->lex_token()) != EOF_TOKEN){};
}

int main(int argc, char **argv){
	if (argc < 2){
		std::printf("[x] Usage: ./%s [filename]\n",argv[0]);
	}	
	cout << "[*] parsing source code '" << argv[1] << "'\n";
	std::string filename(argv[1]);
	Parser parser(filename);
	parser.__base_parser_test();
	//parser.__tokenizer_test();

	return 0;
}
